# 总目录


# 接口测试

## 接口功能测试

- [开源测试服务](https://mp.weixin.qq.com/s/ZOs0cp_vt6_iiundHaKk4g)  ``2020-08-09``
- [使用springboot+mybatis数据库存储服务化](https://mp.weixin.qq.com/s/N_5tHW1JJLZlxCaDI2PvyQ)  ``2019-07-27``
- [alertover推送api的java httpclient实现实例](https://mp.weixin.qq.com/s/DJXCBEG3SbybfbT6blO1jA)  ``2019-08-07``
- [接口自动化通用验证类](https://mp.weixin.qq.com/s/fP1clCKkLREfg6POKV5n1A)  ``2019-12-21``
- [将swagger文档自动变成测试代码](https://mp.weixin.qq.com/s/SY8mVenj0zMe5b47GS9VSQ)  ``2019-08-14``
- [httpclient处理多用户同时在线](https://mp.weixin.qq.com/s/Nuc30Fwy6-Qyr-Pc65t1_g)  ``2019-08-19``
- [使用httpclient实现图灵机器人web api调用实例](https://mp.weixin.qq.com/s/dYyxvAhwSmJkNI8N9lYQfg)  ``2019-08-16``
- [groovy如何使用java接口测试框架发送http请求](https://mp.weixin.qq.com/s/KF5lzMT-E2IBOkp_UjuC4g)  ``2019-08-20``
- [httpclient调用京东万象数字营销频道新闻api实例](https://mp.weixin.qq.com/s/kSqgSbPci-q2pfsdcU5Ekw)  ``2019-08-22``
- [httpclient遇到socket closed解决办法](https://mp.weixin.qq.com/s/mDRC7mssKmnvcI6StQWIBQ)  ``2019-08-26``
- [httpclient4.5如何确保资源释放](https://mp.weixin.qq.com/s/373Lx1bv0vi-pIBgWNzC9Q)  ``2019-08-29``
- [httpclient如何处理302重定向](https://mp.weixin.qq.com/s/vg354AjPKhIZsnSu4GZjZg)  ``2019-09-03``
- [基于java的直线型接口测试框架初探](https://mp.weixin.qq.com/s/xhg4exdb1G18-nG5E7exkQ)  ``2019-09-06``
- [利用alertover发送获取响应失败的通知消息](https://mp.weixin.qq.com/s/w6y2UkgL3J20mAxc8fq0tA)  ``2019-09-08``
- [使用httpclient中EntityUtils类解析entity遇到socket closed错误的原因](https://mp.weixin.qq.com/s/RJnuOa2K6aRCElJafkFeug)  ``2019-09-07``
- [httpclient接口测试中重试控制器设置](https://mp.weixin.qq.com/s/hknNdq_ybQ1MoXh_dI3JVA)  ``2019-09-22``
- [拼接GET请求的参数](https://mp.weixin.qq.com/s/EGw_97scexH_3m2Uc8Ye5A)  ``2019-09-23``
- [httpclient上传文件方法的封装](https://mp.weixin.qq.com/s/HIrwl5ullvEmn_UuyLKkRg)  ``2019-10-05``
- [接口批量上传文件的实例](https://mp.weixin.qq.com/s/wZwkWchXXC6iddX1oVEnZQ)  ``2019-10-12``
- [httpclient发送https协议请求以及javax.net.ssl.SSLHandshakeException解决办法](https://mp.weixin.qq.com/s/uSHhKRrL2f9USKpSykkpkQ)  ``2019-10-16``
- [API测试基础](https://mp.weixin.qq.com/s/bkbUEa9CF21xMYSlhPcULw)  ``2019-12-23``
- [拷贝HttpRequestBase对象](https://mp.weixin.qq.com/s/kxB1c0GmSF5OAM15UQJU2Q)  ``2019-12-26``
- [API自动化测试指南](https://mp.weixin.qq.com/s/uy_Vn_ZVUEu3YAI1gW2T_A)  ``2019-12-28``
- [如何统一接口测试的功能、自动化和性能测试用例](https://mp.weixin.qq.com/s/1xqtXNVw7BdUa03nVcsMTg)  ``2020-01-04``
- [如何选择API测试工具](https://mp.weixin.qq.com/s/m2TNJDiqAAWYV9L6UP-29w)  ``2020-01-05``
- [初学者的API测试技巧](https://mp.weixin.qq.com/s/_uk6dw5Q7CfS-gXGH-TZEQ)  ``2020-01-10``
- [压测中测量异步写入接口的延迟](https://mp.weixin.qq.com/s/odvK1iYgg4eRVtOOPbq15w)  ``2020-02-20``
- [多项目登录互踢测试用例](https://mp.weixin.qq.com/s/Nn_CUy_j7j6bUwHSkO0pCQ)  ``2020-03-27``
- [httpclient使用HTTP代理实践](https://mp.weixin.qq.com/s/24IJwJ1TJWHdfj0PzSjmvw)  ``2020-08-13``
- [HTTP异步连接池和多线程实践](https://mp.weixin.qq.com/s/8M348LuHakBe4GAEnnDPxw)  ``2020-08-20``
- [socket接口开发和测试初探](https://mp.weixin.qq.com/s/uhmkbrMp91PP1pQjlEofOQ)  ``2020-11-17``
- [IntelliJ中基于文本的HTTP客户端](https://mp.weixin.qq.com/s/-9qi_lLVVfxQKEFmcRYFtA)  ``2020-09-23``
- [基于WebSocket的client封装](https://mp.weixin.qq.com/s/1lZvsuGEa6hiRHOgOT-Kmg)  ``2020-11-30``
- [基于Socket.IO的Client封装](https://mp.weixin.qq.com/s/Ux90AXI9g85w7R5i3f9idg)  ``2020-12-03``
- [Socket.IO接口多用户测试实践](https://mp.weixin.qq.com/s/aCLaRZQs8zMK_ptJ-PjClw)  ``2020-12-04``
- [Python版Socket.IO接口测试脚本](https://mp.weixin.qq.com/s/oXBP6Sx3yPqlmvV9uCUScw)  ``2020-12-11``
- [命令行如何执行jar包里面的方法](https://mp.weixin.qq.com/s/50oMEmVEnv5Vzlm1HOxuFw)  ``2020-12-14``
- [JSON对象标记语法验证类](https://mp.weixin.qq.com/s/jSXmoEdMF7nWAqQuzJ5GiQ)  ``2020-12-25``
- [Socket接口异步验证实践](https://mp.weixin.qq.com/s/bnjHK3ZmEzHm3y-xaSVkTw)  ``2020-12-30``
- [无数据驱动自动化测试](https://mp.weixin.qq.com/s/aCYRGxkzMogLbmACYo6ssw)  ``2021-01-03``
- [白板点阵数据传输测试初探](https://mp.weixin.qq.com/s/EzFC-hIvgm7j7947TZU6BA)  ``2021-01-20``
- [基于Socket.IO的白板点阵坐标传输接口测试实践](https://mp.weixin.qq.com/s/pDAx4jwYvcRcdld5cKLAUw)  ``2021-01-22``
- [插上NIO翅膀，FunTester飞上天](https://mp.weixin.qq.com/s/xnprWjuQWn16tO09JquXiA)  ``2021-08-17``
- [Java NIO在接口自动化中应用](https://mp.weixin.qq.com/s/o6zBzAfJkB5SQafLD4m1cg)  ``2021-08-24``
- [线程池处理批量接口请求实践](https://mp.weixin.qq.com/s/iHpqTWn4v77vSp_IuMib4A)  ``2021-10-15``
- [Go WebSocket开发与测试实践【/net/websocket】](https://mp.weixin.qq.com/s/Yj4vD5AYVWcTlmsbSrxHng)  ``2021-11-11``
- [Java自定义DNS解析器三种实践](https://mp.weixin.qq.com/s/eAdYHyOWmrYLcVV38z_K3A)  ``2022-01-24``
- [Java自定义DNS解析器负载均衡实现](https://mp.weixin.qq.com/s/DnP3cufgyLgEnjdECMJlOA)  ``2022-02-10``
- [Go语言自定义DNS解析器实践](https://mp.weixin.qq.com/s/nvglgC4CsvPx6XE9rgSUOw)  ``2022-02-17``
- [Go自定义DNS解析器负载均衡实践](https://mp.weixin.qq.com/s/MOLQxpvakVPfsNul-nxqNg)  ``2022-02-24``
- [Jira API的六种传参方式](https://mp.weixin.qq.com/s/_sZPSHO_eWNut2wK7NaONQ)  ``2022-03-22 ``
- [Grpc服务开发和接口测试初探【Java】](https://mp.weixin.qq.com/s/7HZpgbcDFuOnbPyVY5PfvQ)  `` 2022-04-20``
- [gRPC服务开发和接口测试初探【Go】](https://mp.weixin.qq.com/s/csvLLZ19jPb8FNA6TxVrbw)  ``2022-05-07``
- [gRPC三种客户端类型实践【Java版】](https://mp.weixin.qq.com/s/2YQ2CTUpWWfatViPBCc0Zw)  ``2022-05-11``

# 单元&白盒

- [Maven和Gradle中配置单元测试框架Spock](https://mp.weixin.qq.com/s/kL5keijAAZwmq_DO1NDBtw)  ``2019-11-12``
- [Groovy单元测试框架spock基础功能Demo](https://mp.weixin.qq.com/s/fQCyIyeQANbu2YP2ML6_8Q)  ``2019-11-13``
- [Groovy单元测试框架spock数据驱动Demo](https://mp.weixin.qq.com/s/uCAB7Mxt1JZW229aKp-uVQ)  ``2019-11-17``
- [人生苦短？试试Groovy进行单元测试](https://mp.weixin.qq.com/s/ahyP-YQTzigeq_5N8byC4g)  ``2019-11-19``
- [模糊断言](https://mp.weixin.qq.com/s/OlJpqHkwpY6-yyELvQ9cIw)  ``2019-11-29``
- [使用WireMock进行更好的集成测试](https://mp.weixin.qq.com/s/oMuVZOOQmuxSygJWH2_QHg)  ``2019-12-05``
- [如何测试这个方法--功能篇](https://mp.weixin.qq.com/s/4zrwkc6ccozUGjOGV563dQ)  ``2019-12-07``
- [如何测试这个方法--性能篇](https://mp.weixin.qq.com/s/QXl9_9Bj5c191oxkXmByUA)  ``2019-12-08``
- [单元测试用例](https://mp.weixin.qq.com/s/UFEXJ1aXOvJUYp49iVLr5w)  ``2020-01-06``
- [关于测试覆盖率](https://mp.weixin.qq.com/s/E15D785fkaWH7-YhiE5gPw)  ``2020-01-07``
- [JUnit 5和Selenium基础（一）](https://mp.weixin.qq.com/s/ehBRf7st-OxeuvI_0yW3OQ)  ``2020-01-13``
- [JUnit 5和Selenium基础（二）](https://mp.weixin.qq.com/s/Gt82cPmS2iX-DhKXTXiy8g)  ``2020-01-14``
- [JUnit 5和Selenium基础（三）](https://mp.weixin.qq.com/s/8YkonXTYgAV5-pLs9yEAVw)  ``2020-01-15``
- [浅谈单元测试](https://mp.weixin.qq.com/s/mJM9qXQepSYQ9vLBnBEs3Q)  ``2020-01-22``
- [Spock 2.0 M1版本初探](https://mp.weixin.qq.com/s/nyYh2QzER03kIk-w9P9GNw)  ``2020-02-10``
- [Java并发BUG基础篇](https://mp.weixin.qq.com/s/NR4vYx81HtgAEqH2Q93k2Q)  ``2020-02-17``
- [Java并发BUG提升篇](https://mp.weixin.qq.com/s/GCRRe8hJpe1QJtxq9VBEhg)  ``2020-02-18``
- [集成测试、单元测试、系统测试](https://mp.weixin.qq.com/s/LRkxMasRvmDYRVb0_aybtA)  ``2020-01-27``
- [从单元测试标准中学习](https://mp.weixin.qq.com/s/x0TyMAdPBWYL7JSPAmoQsw)  ``2020-03-01``
- [白盒测试扫盲](https://mp.weixin.qq.com/s/s_FvGZTC42GEjaWzroz1eA)  ``2020-03-15``
- [Mock System.in和检查System.out](https://mp.weixin.qq.com/s/1ly3uXCZsukmIylN6F5GxQ)  ``2020-04-28``
- [单元测试框架spock和Mockito应用](https://mp.weixin.qq.com/s/s21Lts1UnG9HwOEVvgj-uw)  ``2020-05-10``
- [Mockito框架Mock Void方法](https://mp.weixin.qq.com/s/R95wOMVyeDCHm3_Z0S2kqg)  ``2020-08-05``
- [JsonPath工具类单元测试](https://mp.weixin.qq.com/s/1YtUWGk_sTjn9bHwAeT0Ew)  ``2020-09-02``
- [Intellij静态代码扫描插件SpotBugs](https://mp.weixin.qq.com/s/8ivsMNOmT0LDfvcM06IGMg)  ``2020-11-20``
- [SpotBugs注解SuppressWarnings在Java&Groovy中的应用](https://mp.weixin.qq.com/s/R0JoqmAqhUbRSjIJ61h_tg)  ``2020-11-27``
- [Spock框架Mock对象、方法经验总结](https://mp.weixin.qq.com/s/7qXVcbARH60bfIQYfIj1wA)  ``2022-01-18``
- [Spock框架Mock静态资源经验汇总](https://mp.weixin.qq.com/s/cmpxABuE_aZXXtgj55lo6A)  ``2022-01-20``

## 性能测试

- [Linux性能监控软件netdata中文汉化版](https://mp.weixin.qq.com/s/7VG7gHx7FUvsuNtBTJpjWA)  ``2019-07-15``
- [性能测试框架](https://mp.weixin.qq.com/s/3_09j7-5ex35u30HQRyWug)  ``2019-07-23``
- [性能测试框架第二版](https://mp.weixin.qq.com/s/JPyGQ2DRC6EVBmZkxAoVWA)  ``2019-12-04``
- [性能测试框架第三版](https://mp.weixin.qq.com/s/Mk3PoH7oJX7baFmbeLtl_w)  ``2020-02-03``
- [一个时间计数器timewatch辅助性能测试](https://mp.weixin.qq.com/s/-YZ04n2kyfO0q2QaKHX_0Q)  ``2019-07-14``
- [如何在Linux命令行界面愉快进行性能测试](https://mp.weixin.qq.com/s/fwGqBe1SpA2V0lPfAOd04Q)  ``2019-07-24``
- [Mac+httpclient高并发配置实例](https://mp.weixin.qq.com/s/r4a-vGz0pxeZBPPH3phujw)  ``2019-08-15``
- [单点登录性能测试方案](https://mp.weixin.qq.com/s/sv8FnvIq44dFEq63LpOD2A)  ``2019-10-08``
- [如何对消息队列做性能测试](https://mp.weixin.qq.com/s/MNt22aW3Op9VQ5OoMzPwBw)  ``2019-07-18``
- [如何对修改密码接口进行压测](https://mp.weixin.qq.com/s/9CL_6-uZOlAh7oeo7NOpag)  ``2019-07-18``
- [如何对单行多次update接口进行压测](https://mp.weixin.qq.com/s/Ly1Y4iPGgL6FNRsbOTv0sg)  ``2019-10-26``
- [如何对多行单次update接口进行压测](https://mp.weixin.qq.com/s/Fsqw7vlw6K9EKa_XJwGIgQ)  ``2019-10-27``
- [如何获取JVM堆转储文件](https://mp.weixin.qq.com/s/qCg7nsXVvT1q-9yquQOfWA)  ``2019-11-27``
- [性能测试中标记每个请求](https://mp.weixin.qq.com/s/PokvzoLdVf_y9inlVXHJHQ)  ``2020-01-11``
- [如何对N个接口按比例压测](https://mp.weixin.qq.com/s/GZxbH4GjDkk4BLqnUj1_kw)  ``2020-01-12``
- [如何性能测试中进行业务验证](https://mp.weixin.qq.com/s/OEvRy1bS2Yq_w1kGiidmng)  ``2020-01-18``
- [性能测试中记录每一个耗时请求](https://mp.weixin.qq.com/s/VXcp4uIMm8mRgqe8fVhuCQ)  ``2020-01-24``
- [线程安全类在性能测试中应用](https://mp.weixin.qq.com/s/0-Y63wXqIugVC8RiKldHvg)  ``2020-02-09``
- [利用微基准测试修正压测结果](https://mp.weixin.qq.com/s/dmO33qhOBrTByw_NshS-uA)  ``2020-02-09``
- [性能测试如何减少本机误差](https://mp.weixin.qq.com/s/S6b_wwSowVolp1Uu6sEIOA)  ``2020-02-09``
- [服务端性能优化之异步查询转同步](https://mp.weixin.qq.com/s/okYP2aOPfkWj2FjZcAtQNA)  ``2020-02-11``
- [服务端性能优化之双重检查锁](https://mp.weixin.qq.com/s/-bOyHBcqFlJY3c0PEZaWgQ)  ``2020-02-15``
- [多种登录方式定量性能测试方案](https://mp.weixin.qq.com/s/WuZ2h2rr0rNBgEvQVioacA)  ``2020-02-16``
- [性能测试中图形化输出测试数据](https://mp.weixin.qq.com/s/EMvpYIsszdwBJFPIxztTvA)  ``2020-02-18``
- [压测中测量异步写入接口的延迟](https://mp.weixin.qq.com/s/odvK1iYgg4eRVtOOPbq15w)  ``2020-02-20``
- [手机号验证码登录性能测试](https://mp.weixin.qq.com/s/i-j8fJAdcsJ7v8XPOnPDAw)  ``2020-04-14``
- [绑定手机号性能测试](https://mp.weixin.qq.com/s/K5x1t1dKtIT2NKV6k4v5mw)  ``2020-04-19``
- [终止性能测试并输出报告](https://mp.weixin.qq.com/s/II4-UbKDikctmS_vRT-xLg)  ``2020-05-11``
- [CountDownLatch类在性能测试中应用](https://mp.weixin.qq.com/s/uYBPPOjauR2h81l2uKMANQ)  ``2020-08-02``
- [CyclicBarrier类在性能测试中应用](https://mp.weixin.qq.com/s/kvEHX3t_2xpMke9vwOdWrg)  ``2020-08-03``
- [Phaser类在性能测试中应用](https://mp.weixin.qq.com/s/plxNnQq7yNQvHYEGpyY4uA)  ``2020-08-04``
- [如何同时压测创建和删除接口](https://mp.weixin.qq.com/s/NCeoEF3DkEtpdaaQ365I0Q)  ``2020-09-12``
- [固定QPS压测模式探索](https://mp.weixin.qq.com/s/S2h-zEUoik_CWs60RL6g7Q)  ``2020-10-13``
- [固定QPS压测初试](https://mp.weixin.qq.com/s/ySlJmDIH3fFB4qEnL-ueMg)  ``2020-11-05``
- [命令行如何执行jar包里面的方法](https://mp.weixin.qq.com/s/50oMEmVEnv5Vzlm1HOxuFw)  ``2020-12-14``
- [链路压测中如何记录每一个耗时的请求](https://mp.weixin.qq.com/s/8sb5QZcKbBjTxCaXK5ajXA)  ``2020-12-17``
- [Socket接口异步验证实践](https://mp.weixin.qq.com/s/bnjHK3ZmEzHm3y-xaSVkTw)  ``2020-12-30``
- [性能测试中集合点和多阶段同步问题初探](https://mp.weixin.qq.com/s/NlpD1WyMrcG1V5RYfY0Plg)  ``2020-12-31``
- [性能测试中标记请求参数实践](https://mp.weixin.qq.com/s/2FNMU-k_En26FCqWkYpvhQ)  ``2021-01-15``
- [测试模型中理解压力测试和负载测试](https://mp.weixin.qq.com/s/smNLx3malzM3avkrn3EJiA)  ``2021-01-18``
- [重放浏览器单个请求性能测试实践](https://mp.weixin.qq.com/s/a10hxCrIzS4TV9JwmDSI3Q)  ``2021-01-28``
- [重放浏览器多个请求性能测试实践](https://mp.weixin.qq.com/s/Hm1Kpp1PMrZ5rYFW8l2GlA)  ``2021-01-29``
- [重放浏览器请求多链路性能测试实践](https://mp.weixin.qq.com/s/9YSBLAyHVw8Z6IfK-nJTpQ)  ``2021-02-04``
- [性能测试中异步展示测试进度](https://mp.weixin.qq.com/s/AOERJbEc4ATJqhjvnxgQoA)  ``2021-02-01``
- [ThreadLocal在链路性能测试中实践](https://mp.weixin.qq.com/s/3qhNdHHSStELzNraQSpcew)  ``2021-02-07``
- [Socket接口固定QPS性能测试实践](https://mp.weixin.qq.com/s/I9-14L8THxvtX1NJY0KPfw)  ``2021-02-20``
- [单链路性能测试实践](https://mp.weixin.qq.com/s/4xHLP-GZwrNu5cFKdfsB6g)  ``2021-02-24``
- [链路性能测试中参数多样性方法分享](https://mp.weixin.qq.com/s/I1pm0fulNrj_S-YkNz-gEA)  ``2021-03-02``
- [链路测试中参数流转图](https://mp.weixin.qq.com/s/xyo9HXBLoXgLW6MSFH3V6w)  ``2021-03-04``
- [线程同步类CyclicBarrier在性能测试集合点应用](https://mp.weixin.qq.com/s/K2YySxX9T4v_rzbvIbIHJA)  ``2021-03-11``
- [链路压测中各接口性能统计](https://mp.weixin.qq.com/s/Deyop0mMpHrRWj9JTHzayw)  ``2021-03-16``
- [性能测试框架中QPS取样器实现](https://mp.weixin.qq.com/s/4-5WhwwE1oRQ7cMUDv7J2w)  ``2021-03-18``
- [链路压测中的支路问题初探](https://mp.weixin.qq.com/s/9iN9XndRPH4vIgc0-jVpUA)  ``2021-03-21``
- [性能测试误差分析文字版-上](https://mp.weixin.qq.com/s/QA1VmNiJGZbNKTUNkt_05w)  ``2021-03-28``
- [性能测试误差分析文字版-下](https://mp.weixin.qq.com/s/JwT9G910gn3YGyjeBlafgg)  ``2021-03-30``
- [性能测试误差统计实践](https://mp.weixin.qq.com/s/D0KAn8Ch4vOqXA_-pWXWhg)  ``2021-04-01``
- [性能测试软启动初探](https://mp.weixin.qq.com/s/fk7LA7GtqR7wiKRAw2IYFA)  ``2021-04-19``
- [性能测试误差对比研究（一）](https://mp.weixin.qq.com/s/BC49_DLNbdbFGcGDFFuh6Q)  ``2021-04-06``
- [性能测试误差对比研究（二）](https://mp.weixin.qq.com/s/8oq9rSyCgxAiQAYrhHUCkg)  ``2021-04-22``
- [性能测试误差对比研究（三）](https://mp.weixin.qq.com/s/RWfQxlcKnOb3Q2mG5PoITg)  ``2021-05-06``
- [性能测试误差对比研究（四）](https://mp.weixin.qq.com/s/z3vnBfWynZ0knITWmgzEow)  ``2021-05-18``
- [分布式性能测试框架用例方案设想（一）](https://mp.weixin.qq.com/s/qvUD8kRkQa2bWm6Cvk2BoQ)  ``2021-05-11``
- [基于docker的分布式性能测试框架功能验证（一）](https://mp.weixin.qq.com/s/7aa4HN82As3Zmf4DIVwv1A)  ``2021-05-20``
- [高QPS下的固定QPS模型](https://mp.weixin.qq.com/s/95ivpkGT2lvYEQpvSCpxEQ)  ``2021-05-28``
- [分布式性能测试框架用例方案设想（二）](https://mp.weixin.qq.com/s/bs65VVvRoB0AGyyfwSgT0w)  ``2021-06-01``
- [基于docker的分布式性能测试框架功能验证（二）](https://mp.weixin.qq.com/s/49fAcamsteo7yVjO8JmQrg)  ``2021-06-16``
- [分布式性能测试框架单节点内测](https://mp.weixin.qq.com/s/IwEQGC2rOgcT7deCWW4wDw)  ``2021-06-08``
- [分段随机实践—模拟线上流量](https://mp.weixin.qq.com/s/qUPhtG5lZIWx6oYrgwEuag)  ``2021-06-21``
- [性能测试框架对比初探](https://mp.weixin.qq.com/s/w46QciCvh6dPswBA42oiIQ)  ``2021-06-25``
- [性能框架哪家强—JMeter、K6、locust、FunTester横向对比](https://mp.weixin.qq.com/s/BGs3ImdkRGFB-h3fxUktmw)  ``2021-06-29``
- [分布式性能测试框架用例方案设想（三）](https://mp.weixin.qq.com/s/hkSn4g9Z3sfWIV9-dOiS7Q)  ``2021-06-30``
- [基于docker的分布式性能测试框架功能验证（三）](https://mp.weixin.qq.com/s/mBEuEWlrw_gwV0T6S93LKA)  ``2021-07-01``
- [10万QPS，K6、Gatling和FunTester终极对决！](https://mp.weixin.qq.com/s/HZvBPUEaws72hlwb1QXzuw)  ``2021-07-05``
- [单机12万QPS——FunTester复仇记](https://mp.weixin.qq.com/s/4IBaEpj3TEHY_2ZdGOON0g)  ``2021-07-27``
- [分布式请求放大器实现](https://mp.weixin.qq.com/s/JAzVyP2u9WicNXvVamBGYQ)  ``2021-07-30``
- [FunTester框架Redis压测预备](https://mp.weixin.qq.com/s/S58_UD2byWP3UNCZlp5c-w)  ``2021-09-02``
- [FunTester框架Redis性能测试实践](https://mp.weixin.qq.com/s/ReuB40bb8S3AvBTD3qHpAQ)  ``2021-09-14``
- [FunTester框架Redis性能测试之list操作](https://mp.weixin.qq.com/s/WBjPdpc4RNZ-rUXhS9qdSg)  ``2021-09-22``
- [全链路压测流量模型](https://mp.weixin.qq.com/s/nSJKvRgrh87LJjDQRBUtdw)  ``2021-09-16``
- [FunTester框架Redis性能测试之map&INCR](https://mp.weixin.qq.com/s/SJtD4mxOUCSfcSTfhbA5Jw)  ``2021-09-30``
- [量化模拟线上流量实践](https://mp.weixin.qq.com/s/hLkTLcDWhisb-0uVsVvVZA)  ``2021-10-14``
- [下单延迟10s撤单性能测试](https://mp.weixin.qq.com/s/8xx5L5n6TBgITMAuRrVbfA)  ``2021-11-25``
- [Java&Go三种HTTP客户端性能测试](https://mp.weixin.qq.com/s/VoGmNXzHZ5YkUGWfyD53Ow)  ``2021-11-30``
- [MySQL性能测试之insert&delete【FunTester框架】](https://mp.weixin.qq.com/s/dbdiGkRf_3NKfs-C95dRkQ)  ``2021-12-08``
- [MySQL性能测试之select&update【FunTester框架】](https://mp.weixin.qq.com/s/s8UKWXxMGROLjLeja6g5Mg)  ``2021-12-10``
- [Java&Go三种HTTP服务端端性能测试](https://mp.weixin.qq.com/s/d5dk7Hhtag9Ml0OUn3SPgw)  ``2021-12-27``
- [高性能队列Disruptor在测试中应用](https://mp.weixin.qq.com/s/-27X8H7ZKEU-3uAUvF0yTA)  ``2021-12-28``
- [千万级日志回放引擎设计稿](https://mp.weixin.qq.com/s/hkSnyGG5cDPpBsdLaappaw)  ``2021-12-30``
- [Java&Go高性能队列之LinkedBlockingQueue性能测试](https://mp.weixin.qq.com/s/aSLejlGHqhd09M6nyrENMw)  ``2022-01-10``
- [Java&Go高性能队列之Disruptor性能测试](https://mp.weixin.qq.com/s/nr8c5pWMyhMr0Hop2zvhbw)  ``2022-02-14``
- [Java&Go高性能队列之channel性能测试](https://mp.weixin.qq.com/s/BBZnVW_8xAfMD50e-ZvSiw)  ``2022-02-17``
- [性能测试中过滤异常的响应时间](https://mp.weixin.qq.com/s/sxk0N1-6nrQS9KZNVcUhGQ)  `` 2022-03-03``
- [Go语言使用gorm对MySQL进行性能测试](https://mp.weixin.qq.com/s/DmpdUJvDS1lZsfPBQSnGkw)  ``2022-03-13``
- [性能测试中Disruptor框架ExceptionHandler使用分享](https://mp.weixin.qq.com/s/EhyQyw5-fQcQG4r2kDiD3A)  ``2022-03-16``
- [性能测试中的LongAdder](https://mp.weixin.qq.com/s/0V8_nrDC8BElYPwb2JlJ2A)  ``2022-03-28``
- [从Groovy到Java性能](https://mp.weixin.qq.com/s/VlDZh8_gbsqS56Tq37DEiw)  ``2022-04-01``
- [性能测试中QPS取样器和RT取样器](https://mp.weixin.qq.com/s/uEHpMxUfg-yecZuSpvg8WQ)  ``2022-04-25``
- [通用池化框架GenericObjectPool性能测试](https://mp.weixin.qq.com/s/CtLB0D5hAi24OVudvXJpow)  ``2022-06-02``
- [通用池化框架GenericKeyedObjectPool性能测试](https://mp.weixin.qq.com/s/DrmjtSBLFxduauj0ss-mWw)  `` 2022-06-07``

# 语言合集


## Java

- [java一行代码打印心形](https://mp.weixin.qq.com/s/QPSryoSbViVURpSa9QXtpg)  ``2019-07-11``
- [操作的原子性与线程安全](https://mp.weixin.qq.com/s/QU3llkGLepX2VCch8Y9GKw)  ``2019-07-16``
- [快看，i++真的不安全](https://mp.weixin.qq.com/s/-CdWdROKSEq_ZiLX2kWxzA)  ``2019-07-19``
- [原子操作组合与线程安全](https://mp.weixin.qq.com/s/XB5LXucAF5Bo8EkfLZYRmw)  ``2019-07-22``
- [java利用for循环输出正三角新解](https://mp.weixin.qq.com/s/nnMR2177LLVn4u_9s9Fl4g)  ``2019-08-14``
- [在main方法之前，到底执行了什么？](https://mp.weixin.qq.com/s/jWxiCMfwmvRHrjPdRG8ZyQ)  ``2019-08-22``
- [传参传的到底是什么?](https://mp.weixin.qq.com/s/p_pEQwE6h6q7PprkW-kjbg)  ``2019-08-24``
- [json里面put了null会怎么样？](https://mp.weixin.qq.com/s/gQVROe01I3JzIqNdTSHpDQ)  ``2019-08-25``
- [主线程都结束了，为何进程还在执行](https://mp.weixin.qq.com/s/q2v5JU5dtmNEol7I7IVY-Q)  ``2019-09-14``
- [java测试框架如何执行groovy脚本文件](https://mp.weixin.qq.com/s/0GYt1l3_z5-1qzBNl6_PzA)  ``2019-10-24``
- [java用递归筛选法求N以内的孪生质数（孪生素数）](https://mp.weixin.qq.com/s/PSdCb-DrgMPb4WpJJMexmQ)  ``2019-08-27``
- [从JVM堆内存分析验证深浅拷贝](https://mp.weixin.qq.com/s/SdYDnoau1rjjvPC2SUymBg)  ``2019-12-21``
- [如何学习Java基础](https://mp.weixin.qq.com/s/FCPStkYoJF67NYln4Lc6xg)  ``2020-01-01``
- [如何保存HTTPrequestbase和CloseableHttpResponse](https://mp.weixin.qq.com/s/gRY8HRQHCh0PfyS7Q22IwA)  ``2020-01-02``
- [如何在匿名thread子类中保证线程安全](https://mp.weixin.qq.com/s/GCXx_-ummi0JfZQ7GTIxig)  ``2020-01-21``
- [Java服务端两个常见的并发错误](https://mp.weixin.qq.com/s/5VvCox3eY6sQDsuaKB4ZIw)  ``2020-01-26``
- [Java中interface属性和实例方法](https://mp.weixin.qq.com/s/vrKkM6522tgw3v_cL7R8HA)  ``2020-01-31``
- [服务端性能优化之双重检查锁](https://mp.weixin.qq.com/s/-bOyHBcqFlJY3c0PEZaWgQ)  ``2020-02-15``
- [Java并发BUG基础篇](https://mp.weixin.qq.com/s/NR4vYx81HtgAEqH2Q93k2Q)  ``2020-02-17``
- [Java并发BUG提升篇](https://mp.weixin.qq.com/s/GCRRe8hJpe1QJtxq9VBEhg)  ``2020-02-18``
- [性能测试中图形化输出测试数据](https://mp.weixin.qq.com/s/EMvpYIsszdwBJFPIxztTvA)  ``2020-02-18``
- [超大对象导致Full GC超高的BUG分享](https://mp.weixin.qq.com/s/L15-0JW9WK-E005GeOG9WQ)  ``2020-02-21``
- [利用ThreadLocal解决线程同步问题](https://mp.weixin.qq.com/s/VEm8jt3ZUEUdyyeXPC8VvQ)  ``2020-02-24``
- [线程安全集合类中的对象是安全的么？](https://mp.weixin.qq.com/s/WKSuPEfzZCVwjVTcoD0Dyg)  ``2020-02-24``
- [如何使用“dd MM”解析日期](https://mp.weixin.qq.com/s/v9ooAj3dKu53JXgxB482HA)  ``2020-02-27``
- [Java和Groovy正则使用](https://mp.weixin.qq.com/s/DT3BKE3ZcCKf6TLzGc5wbg)  ``2020-03-08``
- [运行越来越快的Java热点代码](https://mp.weixin.qq.com/s/AP0BcDEjDuaouaB0RXJOoQ)  ``2020-03-10``
- [6个重要的JVM性能参数](https://mp.weixin.qq.com/s/b1QnapiAVn0HD5DQU9JrIw)  ``2020-04-01``
- [ArrayList浅、深拷贝](https://mp.weixin.qq.com/s/kYsBzFsCyDPUssdV3MDqLA)  ``2020-05-06``
- [Java性能测试中两种锁的实现](https://mp.weixin.qq.com/s/j9dGFvYzCJ0AGwYUtTrTsw)  ``2020-05-09``
- [测试如何处理Java异常](https://mp.weixin.qq.com/s/H00GWiATOD8QHJu3UewrBw)  ``2020-05-21``
- [创建Java守护线程](https://mp.weixin.qq.com/s/_UjWdvq8QWYTshr4SeniBg)  ``2020-05-25``
- [Lambda表达式在线程安全Map中应用](https://mp.weixin.qq.com/s/zZjB5aOWh4a_k1eoEsR5ww)  ``2020-06-01``
- [Java程序是如何浪费内存的](https://mp.weixin.qq.com/s/w7VF5m5cc0X7LNvqmwGfvg)  ``2020-06-03``
- [Java中的自定义异常](https://mp.weixin.qq.com/s/nspIdxFP9qEDtagGN4gaMQ)  ``2020-06-04``
- [Java文本块](https://mp.weixin.qq.com/s/GwasvpJsd7uLngvCr6KlQw)  ``2020-06-28``
- [CountDownLatch类在性能测试中应用](https://mp.weixin.qq.com/s/uYBPPOjauR2h81l2uKMANQ)  ``2020-08-02``
- [CyclicBarrier类在性能测试中应用](https://mp.weixin.qq.com/s/kvEHX3t_2xpMke9vwOdWrg)  ``2020-08-03``
- [Phaser类在性能测试中应用](https://mp.weixin.qq.com/s/plxNnQq7yNQvHYEGpyY4uA)  ``2020-08-04``
- [Java压缩/解压缩字符串](https://mp.weixin.qq.com/s/7vHNd5dEN93DPUqgS8od_A)  ``2020-07-31``
- [Java删除空字符：Java8 & Java11](https://mp.weixin.qq.com/s/6dlgYgTFZsHuJ4Eaby5eyg)  ``2020-09-08``
- [Java Stream中map和flatMap方法](https://mp.weixin.qq.com/s/0FG2o7VUAG6z8a_0je-1EQ)  ``2020-10-10``
- [泛型类的正确用法](https://mp.weixin.qq.com/s/1azilraonPIZNCnw_9MB5Q)  ``2020-10-21``
- [Java字符串到数组的转换--最后放大招](https://mp.weixin.qq.com/s/iMUYZYkJ5CjykwWqinNm5g)  ``2020-11-02``
- [Java求数组的并集--最后放大招](https://mp.weixin.qq.com/s/bZ93SGakyiRbaRujhx4nvw)  ``2020-11-12``
- [Java计算数组平均值--最后放大招](https://mp.weixin.qq.com/s/dxQaFHu2PyAbOK6jpEgEUQ)  ``2020-11-13``
- [Math.abs()  求绝对值返回负值BUG分享](https://mp.weixin.qq.com/s/RHzExuRqF1XsBtzGKzmgGA)  ``2020-11-25``
- [Java代理模式初探](https://mp.weixin.qq.com/s/SBL_K2PQez3vDHhtAN9NLg)  ``2020-12-29``
- [Socket接口异步验证实践](https://mp.weixin.qq.com/s/bnjHK3ZmEzHm3y-xaSVkTw)  ``2020-12-30``
- [性能测试中异步展示测试进度](https://mp.weixin.qq.com/s/AOERJbEc4ATJqhjvnxgQoA)  ``2021-02-01``
- [Java中的ThreadLocal功能演示](https://mp.weixin.qq.com/s/n92k1JswHKrqT7Y_CD9Q0w)  ``2021-02-06``
- [ThreadLocal在链路性能测试中实践](https://mp.weixin.qq.com/s/3qhNdHHSStELzNraQSpcew)  ``2021-02-07``
- [歪解字符串中连续出现次数最多问题](https://mp.weixin.qq.com/s/xBy4iB4qLd4WQgCsVVuemw)  ``2021-02-10``
- [Java&Groovy下载文件对比](https://mp.weixin.qq.com/s/T9WUynej2yOZhCkDUhaLYw)  ``2021-03-04``
- [线程同步类CyclicBarrier在性能测试集合点应用](https://mp.weixin.qq.com/s/K2YySxX9T4v_rzbvIbIHJA)  ``2021-03-11``
- [Java线程同步三剑客](https://mp.weixin.qq.com/s/dkrYkYsnjuu_DXv4X6bwIg)  ``2021-12-25``
- [Intellij运行Java程序启动等待BUG分享](https://mp.weixin.qq.com/s/Y6-CapeSV9UofwggHu_XbQ)  ``2021-03-31``
- [又双叒叕一行代码：Map按值排序](https://mp.weixin.qq.com/s/D2_Xd_IsLEj2teVCrU9-Hw)  ``2021-07-20``
- [getInteger还是getIntValue，这是一个问题](https://mp.weixin.qq.com/s/1GrgVEU6GEezmcSxOxGqZQ)  ``2021-08-06``
- [Java测试框架九大法宝](https://mp.weixin.qq.com/s/PKK9NJm5UBU2HUFnNV9ysw)  ``2021-08-09``
- [Springboot通过@WebFilter日志双份打印BUG分享](https://mp.weixin.qq.com/s/xPvB8Q-Dl8d8TMNJRJplZA)  ``2021-08-19``
- [Java、Groovy、Python和Golang如何把方法当作参数](https://mp.weixin.qq.com/s/meEIKy0lQsVeI3bMCrkldw)  ``2021-10-12``
- [Java自定义异步功能实践](https://mp.weixin.qq.com/s/Dw4Gtwuivid0bm61pFwEiQ)  ``2021-10-19``
- [利用守护线程隐式关闭线程池](https://mp.weixin.qq.com/s/z1d0BuvovcLg91yzJiWiTw)  ``2021-10-27``
- [Java线程安全ReentrantLock](https://mp.weixin.qq.com/s/aWauJ_uk6b8z5Ob90L1urw)  ``2021-11-05``
- [复杂JSON结构创建语法](https://mp.weixin.qq.com/s/n7NH5oVn4oCQk2aFax2BnQ)  ``2021-11-19``
- [Java Collectors API实践](https://mp.weixin.qq.com/s/-uo6ph4vldzxGPFKUrIlCw)  ``2021-11-24``
- [利用Java反射处理private变量](https://mp.weixin.qq.com/s/_wVJ1bNHqfz2FtdwLKiY7g)  ``2021-12-15``
- [利用闭包实现自定义等待方法](https://mp.weixin.qq.com/s/cSQ99bPVBbpnJnjyD4r6CQ)  ``2022-01-06``
- [Java自定义DNS解析器负载均衡实现](https://mp.weixin.qq.com/s/DnP3cufgyLgEnjdECMJlOA)  ``2022-02-10``
- [性能测试中Disruptor框架shutdown失效的问题分享](https://mp.weixin.qq.com/s/otpkkK5IMgpmG9UXeqIRPg)  ``2022-03-02``
- [从Groovy到Java性能](https://mp.weixin.qq.com/s/VlDZh8_gbsqS56Tq37DEiw)  ``2022-04-01``
- [通用池化框架commons-pool2实践](https://mp.weixin.qq.com/s/2KHub-CdkA4hPM4ing9ZKg) ``2022-05-12``
- [通用池化框架实践之GenericKeyedObjectPool](https://mp.weixin.qq.com/s/es0Me6J8F8jHvhjSAq5yzg)  ``2022-05-23``

## Groovy

- [java和groovy混合编程时提示找不到符合错误解决办法](https://mp.weixin.qq.com/s/dLC2W7nIi5zCuK6JTkiA-w)  ``2019-08-12``
- [groovy使用stream语法递归筛选法求N以内的质数](https://mp.weixin.qq.com/s/TsrVn1cuQUrU6wj9OnR-FQ)  ``2019-09-16``
- [使用Groovy进行Bash（shell）操作](https://mp.weixin.qq.com/s/fgCTlZUF3QeNj6jzq1ZgGg)  ``2019-10-10``
- [使用Groovy和Gradle轻松进行数据库操作](https://mp.weixin.qq.com/s/lwmclrnW0csykVRhu7dNTQ)  ``2019-11-16``
- [愉快地使用Groovy Shell](https://mp.weixin.qq.com/s/fJh7fbB3naBFBEiaS62oxw)  ``2019-11-22``
- [Gradle+Groovy基础篇](https://mp.weixin.qq.com/s/c2j7G-PoNtAB3oYYDUhCGw)  ``2019-12-12``
- [Gradle+Groovy提高篇](https://mp.weixin.qq.com/s/yXmYj_1fynLkR0-5FV_Arw)  ``2019-12-13``
- [Groovy重载操作符](https://mp.weixin.qq.com/s/4jW06Q4_vjFR9DovRTTuHg)  ``2020-01-08``
- [用Groovy处理JMeter断言和日志](https://mp.weixin.qq.com/s/Q4yPA4p8dZYAARZ60ZDh9w)  ``2020-03-03``
- [用Groovy处理JMeter变量](https://mp.weixin.qq.com/s/BxtweLrBUptM8r3LxmeM_Q)  ``2020-03-04``
- [用Groovy在JMeter中执行命令行](https://mp.weixin.qq.com/s/VTip7tiLpwBOr1gUoZ0n8A)  ``2020-03-05``
- [用Groovy处理JMeter中的请求参数](https://mp.weixin.qq.com/s/9pCUOXWpMwXR5ynvCMYJ7A)  ``2020-03-06``
- [用Groovy记录JMeter请求和响应](https://mp.weixin.qq.com/s/Hr216Hi3SeSt3_HBpEzl5Q)  ``2020-03-07``
- [Java和Groovy正则使用](https://mp.weixin.qq.com/s/DT3BKE3ZcCKf6TLzGc5wbg)  ``2020-03-08``
- [Groovy中的元组](https://mp.weixin.qq.com/s/0-ka0-tv1vyKbiA6m44jRw)  ``2020-03-09``
- [从Java到Groovy的八级进化论](https://mp.weixin.qq.com/s/QTrRHsD3w-zLGbn79y8yUg)  ``2020-03-11``
- [用Groovy在JMeter中使用正则提取赋值](https://mp.weixin.qq.com/s/9riPpnQZCfKGscuzOOpYmQ)  ``2020-03-08``
- [Groovy在JMeter中处理cookie](https://mp.weixin.qq.com/s/DCnDjWaj2aiKv5HVw3-n6A)  ``2020-03-22``
- [Groovy在JMeter中处理header](https://mp.weixin.qq.com/s/juY-1jEWODJ5HHiEsxhIEw)  ``2020-03-24``
- [Groovy的神奇NullObject](https://mp.weixin.qq.com/s/jLGisN_30PrCgNP33Sww0g)  ``2020-04-12``
- [Groovy中的list](https://mp.weixin.qq.com/s/0mUe1_WrUiEm1t6kqCV3eQ)  ``2020-04-15``
- [JMeter参数签名——Groovy脚本形式](https://mp.weixin.qq.com/s/wQN9-xAUQofSqiAVFXdqug)  ``2020-04-17``
- [Groovy中的闭包](https://mp.weixin.qq.com/s/pfcG47gSPfUveAaEfdeo8A)  ``2020-04-21``
- [JMeter参数签名——Groovy工具类形式](https://mp.weixin.qq.com/s/urwU4p9ofv9sU-JFy5Z0iA)  ``2020-04-24``
- [删除List中null的N种方法--最后放大招](https://mp.weixin.qq.com/s/4mfskN781dybyL59dbSbeQ)  ``2020-04-26``
- [混合Java函数和Groovy闭包](https://mp.weixin.qq.com/s/FAIzGgLSX2u7RKbOGs3lGA)  ``2020-04-29``
- [Groovy重载操作符（终极版）](https://mp.weixin.qq.com/s/4oYGJ2B2Y1AqxsIj8v5nZA)  ``2020-08-19``
- [JsonPath工具类单元测试](https://mp.weixin.qq.com/s/1YtUWGk_sTjn9bHwAeT0Ew)  ``2020-09-02``
- [Groovy小记it关键字和IDE报错](https://mp.weixin.qq.com/s/cIMHzkvKtH0a0ewkiBnV8g)  ``2020-09-18``
- [JsonPath验证类既Groovy重载操作符实践](https://mp.weixin.qq.com/s/5gc04CAsBY6pWxe5c2P41w)  ``2020-09-19``
- [Groovy枚举类初始化异常分析](https://mp.weixin.qq.com/s/koFhpBZM1MFYYxCNxUKPyQ)  ``2021-01-04``
- [Java&Groovy下载文件对比](https://mp.weixin.qq.com/s/T9WUynej2yOZhCkDUhaLYw)  ``2021-03-04``
- [反射执行Groovy类方法NoSuchMethodException解答](https://mp.weixin.qq.com/s/N0taBoB27CUXQ0vidK_55Q)  ``2021-04-26``
- [Groovy反射invokeMethod传参实践](https://mp.weixin.qq.com/s/ltQqLg6_8V-WxsUh1ZslAQ)  ``2021-04-29``
- [Groovy参数默认值在接口测试中应用](https://mp.weixin.qq.com/s/xmnmWHd_u0gBc4BxplYPuA)  ``2021-09-23``
- [Groovy入门常用语法](https://mp.weixin.qq.com/s/lIeHPxvcDWQYOs0Y_NZ5jQ)  ``2021-10-07``
- [Java、Groovy、Python和Golang如何把方法当作参数](https://mp.weixin.qq.com/s/meEIKy0lQsVeI3bMCrkldw)  ``2021-10-12``
- [Groovy动态添加方法和属性及Spock单测](https://mp.weixin.qq.com/s/OHmm9vTNrtQo571HxIUPuw)  ``2021-10-22``
- [给JSONObject添加自定义遍历方法](https://mp.weixin.qq.com/s/tsgvdFwDegP47s_MQK68Ug)  ``2021-10-28``
- [Groovy热更新Java实践](https://mp.weixin.qq.com/s/QZn2dmBDdsNxkgQPRukBtw)  ``2021-12-01``
- [利用闭包实现自定义等待方法](https://mp.weixin.qq.com/s/cSQ99bPVBbpnJnjyD4r6CQ)  ``2022-01-06``
- [从Groovy到Java性能](https://mp.weixin.qq.com/s/VlDZh8_gbsqS56Tq37DEiw)  ``2022-04-01``
- [Groovy踩坑记之方法调用八层认识](https://mp.weixin.qq.com/s/cokuY4AxFXfTb5U49H30vg)  ``2022-04-13``

## Python

- [python使用filter方法递归筛选法求N以内的质数（素数）--附一行打印心形标记的代码解析](https://mp.weixin.qq.com/s/D8RfpdIi8smCL8TAzBcNpA)  ``2019-07-30``
- [关于python版微信使用经验分享](https://mp.weixin.qq.com/s/19IaI6ETZAm_T4ePPlXqIg)  ``2019-07-30``
- [python用递归筛选法求N以内的孪生质数（孪生素数）](https://mp.weixin.qq.com/s/rVY2pTl8So11WCvA9GrFbA)  ``2019-09-08``
- [利用python wxpy和requests写一个自动应答微信机器人实例](https://mp.weixin.qq.com/s/Fni2kX5BRjdqOQ-glCLjRg)  ``2019-09-13``
- [Python版Socket.IO接口测试脚本](https://mp.weixin.qq.com/s/oXBP6Sx3yPqlmvV9uCUScw)  ``2020-12-11``
- [Java、Groovy、Python和Golang如何把方法当作参数](https://mp.weixin.qq.com/s/meEIKy0lQsVeI3bMCrkldw)  ``2021-10-12``

## Golang

- [Golang语言HTTP客户端实践](https://mp.weixin.qq.com/s/e8kjdeI6WzMhNYnC9X2oRg)  ``2021-10-08``
- [Java、Groovy、Python和Golang如何把方法当作参数](https://mp.weixin.qq.com/s/meEIKy0lQsVeI3bMCrkldw)  ``2021-10-12``
- [Golang fasthttp实践](https://mp.weixin.qq.com/s/9otWDl2mSGEo9fboyToNnw)  ``2021-10-20``
- [Go语言HTTPServer开发的六种实现](https://mp.weixin.qq.com/s/Fy_Q-tAnSDwuQMzoE9ZkVw)  ``2021-11-01``
- [Go WebSocket开发与测试实践【/net/websocket】](https://mp.weixin.qq.com/s/Yj4vD5AYVWcTlmsbSrxHng)  ``2021-11-11``
- [LevelDB Java&Go实践](https://mp.weixin.qq.com/s/-IlML_QyWMAMTCJAG0EoSA)  ``2021-11-16``
- [Go WebSocket开发与测试实践【gorilla/websocket】](https://mp.weixin.qq.com/s/cAR6IqUkLfQI5JF56aLa4Q)  ``2021-11-23``
- [Java&Go三种HTTP客户端性能测试](https://mp.weixin.qq.com/s/VoGmNXzHZ5YkUGWfyD53Ow)  ``2021-11-30``
- [Go语言gorm框架MySQL实践](https://mp.weixin.qq.com/s/Gi6siNbsR0aM9d8MeKm97w)  ``2022-01-11``
- [Java&Go高性能队列之channel性能测试](https://mp.weixin.qq.com/s/BBZnVW_8xAfMD50e-ZvSiw)  ``2022-02-17``
- [Go语言自定义DNS解析器实践](https://mp.weixin.qq.com/s/nvglgC4CsvPx6XE9rgSUOw)  ``2022-02-17``
- [Go自定义DNS解析器负载均衡实践](https://mp.weixin.qq.com/s/MOLQxpvakVPfsNul-nxqNg)  ``2022-02-24``
- [Go语言使用gorm对MySQL进行性能测试](https://mp.weixin.qq.com/s/DmpdUJvDS1lZsfPBQSnGkw)  ``2022-03-13``
- [gRPC服务开发和接口测试初探【Go】](https://mp.weixin.qq.com/s/csvLLZ19jPb8FNA6TxVrbw)  ``2022-05-07``

## FunTester笔记

- [我的开发日记（一）](https://mp.weixin.qq.com/s/eQgpOKbXsU9vOmxp0Xiklg)  ``2020-06-15``
- [我的开发日记（二）](https://mp.weixin.qq.com/s/XuffL3ZmKKOgHDtH_cEYOw)  ``2020-06-16``
- [我的开发日记（三）](https://mp.weixin.qq.com/s/a-I0agh6nWp8RLlcmbgf5w)  ``2020-06-17``
- [我的开发日记（四）](https://mp.weixin.qq.com/s/QukXd00Mx_dbkgiXys0FNg)  ``2020-06-18``
- [我的开发日记（五）](https://mp.weixin.qq.com/s/6P3nScsVW6MfMcyIqcA1AQ)  ``2020-06-19``
- [我的开发日记（六）](https://mp.weixin.qq.com/s/Gz2QmukONNldSy9Fd29u5w)  ``2020-06-20``
- [我的开发日记（七）](https://mp.weixin.qq.com/s/MjZ-nFXfQkHMsXS0fX1c1w)  ``2020-06-21``
- [我的开发日记（八）](https://mp.weixin.qq.com/s/6ZhNcFm-gR5dhKQjEkE3Rg)  ``2020-06-23``
- [我的开发日记（九）](https://mp.weixin.qq.com/s/VfD2T3orojGxnylr3Q5UeA)  ``2020-06-25``
- [我的开发日记（十）](https://mp.weixin.qq.com/s/6DWth40LGbAraJi05G16Pw)  ``2020-06-30``
- [我的开发日记（十一）](https://mp.weixin.qq.com/s/nsX5A-P6QbePHDN_Pse0_A)  ``2020-07-01``
- [我的开发日记（十二）](https://mp.weixin.qq.com/s/XA1KJXBP3Zl-XFswXxUtvg)  ``2020-07-03``
- [我的开发日记（十三）](https://mp.weixin.qq.com/s/_QPUu5pUlg4A_AlC5wOGkA)  ``2020-07-09``
- [我的开发日记（十四）](https://mp.weixin.qq.com/s/Qy1YKAb3wqW_Ip2FwH7Otw)  ``2020-07-12``
- [我的开发日记（十五）](https://mp.weixin.qq.com/s/bwkvz2t6YItQD0O_BIxpHQ)  ``2020-07-15``
- [这些年，我写过的BUG（一）](https://mp.weixin.qq.com/s/mVTmT1FdwWl1e0BaL7Ne1g)  ``2020-07-22``
- [这些年，我写过的BUG（二）](https://mp.weixin.qq.com/s/NMz5n0ZMf6taGb-gr1BLyw)  ``2020-07-30``
- [FunTester测试框架架构图初探](https://mp.weixin.qq.com/s/bcMbVDkWbHSXjZFDeFyJsQ)  ``2021-02-16``
- [FunTester测试项目架构图初探](https://mp.weixin.qq.com/s/wqb8FXRbEXrhDuZounmNXA)  ``2021-02-18``
- [FunTester moco server框架架构图](https://mp.weixin.qq.com/s/xVNB3hlVm20HCeAxNJoW8A)  ``2021-06-03``
- [DCS_FunTester分布式压测框架更新（一）](https://mp.weixin.qq.com/s/mT-traL5EbUxZ7K_qZSyew)  ``2021-07-07``
- [DCS_FunTester分布式压测框架更新（二）](https://mp.weixin.qq.com/s/P4HY6xNBEE-8Yuq9wrmCTA)  ``2021-08-12``
- [DCS_FunTester分布式压测框架更新（三）](https://mp.weixin.qq.com/s/L3ICSd9uy3p3oTuUMRu7_Q)  ``2021-08-20``
- [环境基础【FunTester框架教程】](https://mp.weixin.qq.com/s/YnHuH88Ib3NAW_4z239HAA)  ``2021-08-11``
- [HTTP接口测试基础【FunTester框架教程】](https://mp.weixin.qq.com/s/xnjI5tqTobd4_3seBDvOHQ)  ``2021-08-25``
- [超万字回顾FunTester的前世今生](https://mp.weixin.qq.com/s/bEW6DK86qwgAWnjdkUGiZA)  ``2021-08-03``
- [居家费拖鞋【FunTester居家日记】](https://mp.weixin.qq.com/s/9hNE3uuv-KArYvrHBowCxQ)  ``2021-08-26``
- [一起吐槽接口文档](https://mp.weixin.qq.com/s/kdFx8gpqX4sVDW5HbcLx1g)  ``2021-08-31``
- [变怪OR变菜【FunTester居家日记】](https://mp.weixin.qq.com/s/wN6EGqrxUbrnmVybgJhR4Q)  ``2021-09-03``
- [Jira API的踩坑记](https://mp.weixin.qq.com/s/Mw9AzcK8tlceXEgaKqLVeg)  ``2021-09-10``
- [FunTester抄代码之路](https://mp.weixin.qq.com/s/aY9njB17FIGq8wuLL7ICeg)  ``2021-09-13``
- [动态模型之增压暂停【FunTester测试框架】](https://mp.weixin.qq.com/s/HRLhZ98dI9yxYEmCIEd6Xg)  ``2021-10-13``
- [DCS_FunTester分布式性能测试框架分享](https://mp.weixin.qq.com/s/oQMZuLbt6-SWSNLhTDltFQ)  ``2021-10-16``
- [动态模型之动态增减【FunTester测试框架】](https://mp.weixin.qq.com/s/nSILD4EsnvQElua6Z109Ow)  ``2021-11-18``
- [2020年FunTester自我总结](https://mp.weixin.qq.com/s/DeDY1JZUTk3cjjQfr3DJRg)  ``2020-12-22``
- [FunTester2021年总结](https://mp.weixin.qq.com/s/xB67Pny6PFGtV3tAXpDZhA)  ``2021-12-31``


# 案例分享


## 测试方案

- [如何对消息队列做性能测试](https://mp.weixin.qq.com/s/MNt22aW3Op9VQ5OoMzPwBw)  ``2019-07-18``
- [如何对修改密码接口进行压测](https://mp.weixin.qq.com/s/9CL_6-uZOlAh7oeo7NOpag)  ``2019-07-18``
- [如何测试概率型业务接口](https://mp.weixin.qq.com/s/kUVffhjae3eYivrGqo6ZMg)  ``2019-07-29``
- [如何测试非固定型概率算法P=p(1+0.1*N)  ](https://mp.weixin.qq.com/s/sgg8v-Bi-_sUDJXwuTCMGg)  ``2019-08-31``
- [性能测试中标记每个请求](https://mp.weixin.qq.com/s/PokvzoLdVf_y9inlVXHJHQ)  ``2020-01-11``
- [如何对N个接口按比例压测](https://mp.weixin.qq.com/s/GZxbH4GjDkk4BLqnUj1_kw)  ``2020-01-12``
- [多种登录方式定量性能测试方案](https://mp.weixin.qq.com/s/WuZ2h2rr0rNBgEvQVioacA)  ``2020-02-16``
- [压测中测量异步写入接口的延迟](https://mp.weixin.qq.com/s/odvK1iYgg4eRVtOOPbq15w)  ``2020-02-20``
- [绑定手机号性能测试](https://mp.weixin.qq.com/s/K5x1t1dKtIT2NKV6k4v5mw)  ``2020-04-19``
- [手机号验证码登录性能测试](https://mp.weixin.qq.com/s/i-j8fJAdcsJ7v8XPOnPDAw)  ``2020-04-14``
- [重放浏览器单个请求性能测试实践](https://mp.weixin.qq.com/s/a10hxCrIzS4TV9JwmDSI3Q)  ``2021-01-28``
- [重放浏览器多个请求性能测试实践](https://mp.weixin.qq.com/s/Hm1Kpp1PMrZ5rYFW8l2GlA)  ``2021-01-29``
- [重放浏览器请求多链路性能测试实践](https://mp.weixin.qq.com/s/9YSBLAyHVw8Z6IfK-nJTpQ)  ``2021-02-04``
- [Socket接口固定QPS性能测试实践](https://mp.weixin.qq.com/s/I9-14L8THxvtX1NJY0KPfw)  ``2021-02-20``

## BUG集锦

- [一个MySQL索引引发的血案](https://mp.weixin.qq.com/s/KLSber-gPg53JVfsCa3Dtw)  ``2019-07-31``
- [微软Zune闰年BUG分析](https://mp.weixin.qq.com/s/zpqAUcNcHaZjWUdUYH_loQ)  ``2019-08-02``
- [“双花”BUG的测试分享](https://mp.weixin.qq.com/s/0dsBsssNfg-seJ_tu9zFaQ)  ``2019-08-12``
- [iOS 11计算器1+2+3=24真的是bug么？](https://mp.weixin.qq.com/s/nokQhe_Hqcq-o7pZJmFlqQ)  ``2019-08-13``
- [不要在遍历的时候删除](https://mp.weixin.qq.com/s/MIczbEpbOrADL0_V7ZUhlg)  ``2019-08-29``
- [连开100年会员会怎样](https://mp.weixin.qq.com/s/mZw-SFIxFFbE-o8UeXhdfg)  ``2019-09-28``
- [异步查询转同步加redis业务实现的BUG分享](https://mp.weixin.qq.com/s/ni3f6QTxw0K-0I3epvEYOA)  ``2020-01-25``
- [Java服务端两个常见的并发错误](https://mp.weixin.qq.com/s/5VvCox3eY6sQDsuaKB4ZIw)  ``2020-01-26``
- [超大对象导致Full GC超高的BUG分享](https://mp.weixin.qq.com/s/L15-0JW9WK-E005GeOG9WQ)  ``2020-02-21``
- [访问权限导致toString返回空BUG分享](https://mp.weixin.qq.com/s/usDOcuJrXOmEKN-mVBzRKg)  ``2020-10-09``
- [异常使用中的BUG](https://mp.weixin.qq.com/s/IG9Ar3IT7CrlSv4d0lCvgA)  ``2020-11-11``
- [Math.abs()  求绝对值返回负值BUG分享](https://mp.weixin.qq.com/s/RHzExuRqF1XsBtzGKzmgGA)  ``2020-11-25``

## 爬虫实践

- [接口爬虫之网页表单数据提取](https://mp.weixin.qq.com/s/imJ5u67xhYQaEzv-O1in4g)  ``2019-09-12``
- [httpclient爬虫爬取汉字拼音等信息](https://mp.weixin.qq.com/s/w-IvBxAsotmPA3pydpIo1w)  ``2019-09-13``
- [httpclient爬虫爬取电影信息和下载地址实例](https://mp.weixin.qq.com/s/TB49X4S-ioyoW5CrzAnHcw)  ``2019-09-15``
- [httpclient 多线程爬虫实例](https://mp.weixin.qq.com/s/nXL-MP4Y6CN2hgZQefWEeQ)  ``2019-09-17``
- [groovy爬虫练习之——企业信息](https://mp.weixin.qq.com/s/1TisDceIL1-Luqz_wOqAiw)  ``2019-10-09``
- [httpclient 爬虫实例——爬取三级中学名](https://mp.weixin.qq.com/s/Dd7U30aHYauqBFxJdxaiyg)  ``2019-10-14``
- [电子书网站爬虫实践](https://mp.weixin.qq.com/s/KGW0dIS5NTLgxyhSjxDiOw)  ``2019-10-17``
- [groovy爬虫实例——历史上的今天](https://mp.weixin.qq.com/s/5LDUvpU6t_GZ09uhZr224A)  ``2019-10-19``
- [爬取720万条城市历史天气数据](https://mp.weixin.qq.com/s/vOyKpeGlJSJp9bQ8NIMe2A)  ``2019-10-22``
- [记一次失败的爬虫](https://mp.weixin.qq.com/s/SMylrZLXDGw5f1xKI9ObnA)  ``2020-02-04``
- [爬虫实践--CBA历年比赛数据](https://mp.weixin.qq.com/s/mM_QSQddabU5im_O6iVR-Q)  ``2019-11-14``
- [图片爬虫实践](https://mp.weixin.qq.com/s/u5bRSyKsmn3TcjqEEqRJpw)  ``2021-03-09``
- [微信公众号文章爬虫实践](https://mp.weixin.qq.com/s/GhQQ_FMUp8Z6Q5viUdKqUQ)  ``2022-02-08``

# 工具合集


## JSON合集

- [JsonPath实践（一）](https://mp.weixin.qq.com/s/Cq0_v_ptbGd4f5y8HIsq7w)  ``2020-08-08``
- [JsonPath实践（二）](https://mp.weixin.qq.com/s/w_iJTiuQahIw6U00CJVJZg)  ``2020-08-14``
- [JsonPath实践（三）](https://mp.weixin.qq.com/s/58A3k0T6dbOkBJ5nRYKDqA)  ``2020-08-16``
- [JsonPath实践（四）](https://mp.weixin.qq.com/s/8ER61qrkMj8bdBpyuq9r6w)  ``2020-08-18``
- [JsonPath实践（五）](https://mp.weixin.qq.com/s/knVLW960WXnckGLstdrOVQ)  ``2020-08-21``
- [JsonPath实践（六）](https://mp.weixin.qq.com/s/ckBCK3t1w68FLBhaw5a7Jw)  ``2020-08-28``
- [JsonPath工具类封装](https://mp.weixin.qq.com/s/KyuCuG5fVEExxBdGJO2LdA)  ``2020-08-31``
- [JsonPath工具类单元测试](https://mp.weixin.qq.com/s/1YtUWGk_sTjn9bHwAeT0Ew)  ``2020-09-02``
- [JsonPath验证类既Groovy重载操作符实践](https://mp.weixin.qq.com/s/5gc04CAsBY6pWxe5c2P41w)  ``2020-09-19``
- [JSON对象标记语法验证类](https://mp.weixin.qq.com/s/jSXmoEdMF7nWAqQuzJ5GiQ)  ``2020-12-25``
- [使用jq处理JSON数据（一）](https://mp.weixin.qq.com/s/45-ztTx2scbNY5u7NQzeIA)  ``2021-03-23``
- [使用jq处理JSON数据（二）](https://mp.weixin.qq.com/s/U1VwJGFVkrUyv3jMegapCg)  ``2021-04-08``
- [使用jq处理JSON数据（三）](https://mp.weixin.qq.com/s/NdCb0FKJhxHMAj3HgeMoTQ)  ``2021-04-28``

## Jacoco覆盖率

- [接口测试代码覆盖率（jacoco）方案分享](https://mp.weixin.qq.com/s/D73Sq6NLjeRKN8aCpGLOjQ)  ``2019-07-15``
- [jacoco无法读取build.xml配置中源码路径解决办法](https://mp.weixin.qq.com/s/8_x0rVfkIi-uX3y0drx_jw)  ``2019-07-25``
- [使用JaCoCo Maven插件创建代码覆盖率报告](https://mp.weixin.qq.com/s/4Jo05k2WxytiSSNW9WTV-A)  ``2019-11-18``
- [Java 8，Jenkins，Jacoco和Sonar进行持续集成](https://mp.weixin.qq.com/s/dOoXnKnWtQmmC5itClsl4g)  ``2019-11-20``
- [jacoco测试覆盖率过滤非业务类](https://mp.weixin.qq.com/s/7YGe9pCHw3wd87tgOlKjSA)  ``2020-05-22``

## moco API

- [解决moco框架API在post请求json参数情况下query失效的问题](https://mp.weixin.qq.com/s/V5lXoepEBtPJrSUHA0Uz5A)  ``2019-05-30``
- [给moco API添加limit功能](https://mp.weixin.qq.com/s/pXJECi15ieNLmA0uIqEqfA)  ``2019-07-17``
- [给moco API添加random功能](https://mp.weixin.qq.com/s/YTcbFbFaWB5arW_fubgTTQ)  ``2019-08-11``
- [解决moco框架API在cycle方法缺失的问题](https://mp.weixin.qq.com/s/YfsPa7eW8WV65CDbPooBPg)  ``2019-08-11``
- [五行代码构建静态博客](https://mp.weixin.qq.com/s/hZnimJOg5OqxRSDyFvuiiQ)  ``2019-09-02``
- [moco API模拟框架视频讲解（上）](https://mp.weixin.qq.com/s/X5-fFXe018_O60WCRdawZg)  ``2020-05-15``
- [moco API模拟框架视频讲解（中）](https://mp.weixin.qq.com/s/g2En-9W9JWYrCLQr_WPEBA)  ``2020-05-17``
- [moco API模拟框架视频讲解（下）](https://mp.weixin.qq.com/s/mz__DiNxMGHwIKCLsjKR8g)  ``2020-05-20``
- [如何mock固定QPS的接口](https://mp.weixin.qq.com/s/yogj9Fni0KJkyQuKuDYlbA)  ``2020-05-27``
- [mock延迟响应的接口](https://mp.weixin.qq.com/s/x_fu0InQpYIUJIQFi9a50g)  ``2020-05-30``
- [moco固定QPS接口升级补偿机制](https://mp.weixin.qq.com/s/zAM91e_REo4edSPTLuHLOw)  ``2020-09-04``
- [moco框架接口命中率统计实践](https://mp.weixin.qq.com/s/Fq7Sa6nu_GETMpEKy8zUZw)  ``2021-05-24``

## 工具类

- [java使用poi写入excel文档的一种解决方案](https://mp.weixin.qq.com/s/Ft56gd1B9CPrQs2zq4Cpug)  ``2019-08-04``
- [java使用poi读取excel文档的一种解决方案](https://mp.weixin.qq.com/s/ltZGx9J7E8DTer0D-pfQ2Q)  ``2019-08-04``
- [MongoDB操作类封装](https://mp.weixin.qq.com/s/u-RHOE5XrjOEkelWIxdplw)  ``2019-08-06``
- [java网格输出的类](https://mp.weixin.qq.com/s/QW8nKM2Bz7C75fdkCzSbpw)  ``2019-08-08``
- [将json数据格式化输出到控制台](https://mp.weixin.qq.com/s/2IPwvh-33Ov2jBh0_L8shA)  ``2019-08-08``
- [利用反射根据方法名执行方法的使用示例](https://mp.weixin.qq.com/s/5ntwDo4ZVcTh1PmK4vkNfA)  ``2019-08-17``
- [解决统计出现次数问题的方法类](https://mp.weixin.qq.com/s/gqz4wuKkMWAOIQwMtiupnA)  ``2019-08-23``
- [java利用时间戳来获取UTC时间](https://mp.weixin.qq.com/s/wbDIrwDnxb9_XWkkmP3A_g)  ``2019-08-22``
- [如何遍历执行一个包里面每个类的用例方法](https://mp.weixin.qq.com/s/OJwCOHCJ4TalatsEWbtzIQ)  ``2019-08-24``
- [阿拉伯数字转成汉字](https://mp.weixin.qq.com/s/jNZXIvwMpdxt7jIAlVBgHg)  ``2019-09-29``
- [获取JVM转储文件的Java工具类](https://mp.weixin.qq.com/s/f_TlOb3m8MeR3argBmTzzA)  ``2019-12-09``
- [基于DOM的XML文件解析类](https://mp.weixin.qq.com/s/scRj7OAhvJYL3mx_hCFp4A)  ``2020-09-29``
- [XML文件解析实践（DOM解析）](https://mp.weixin.qq.com/s/V2DG3osaPNUJzFNDQgqM-w)  ``2020-09-30``
- [基于DOM4J的XML文件解析类](https://mp.weixin.qq.com/s/K5R7iMXouTn4g0p14T7iAQ)  ``2020-10-11``
- [将HTTP请求对象转成curl命令行](https://mp.weixin.qq.com/s/861uMAMMWtINjy4Z99WA6w)  ``2021-03-10``
- [控制台彩色输出](https://mp.weixin.qq.com/s/QiwJfQoBMfgN3ST1cSIbAg)  ``2021-12-07``
- [LevelDB Java&Go实践](https://mp.weixin.qq.com/s/-IlML_QyWMAMTCJAG0EoSA)  ``2021-11-16``
- [LevelDB封装和功能拓展](https://mp.weixin.qq.com/s/iP811G6UIMbF25GRkle5qQ)  ``2021-12-16``
- [LevelDB在测试中应用应用](https://mp.weixin.qq.com/s/fAPWU6TJya58oGO291jO4A)  ``2022-01-04``

## 构建工具

- [java和groovy混编的Maven项目如何用intellij打包执行jar包](https://mp.weixin.qq.com/s/bKexZXlONeo3r6FDhfMltQ)  ``2019-07-26``
- [window系统权限不足导致gradle构建失败的解决办法](https://mp.weixin.qq.com/s/dqiQvmVG1o6glU-pknLDwQ)  ``2019-08-01``
- [使用groovy脚本使gradle灵活加载本地jar包的两种方式](https://mp.weixin.qq.com/s/p3K3ZS7iOUeKO7E94gKFVg)  ``2019-08-24``
- [Java 8，Jenkins，Jacoco和Sonar进行持续集成](https://mp.weixin.qq.com/s/dOoXnKnWtQmmC5itClsl4g)  ``2019-11-20``
- [Gradle如何在任务失败后继续构建](https://mp.weixin.qq.com/s/GcXDzRN7cM_QQpt9ytqoKg)  ``2019-11-25``
- [Gradle+Groovy基础篇](https://mp.weixin.qq.com/s/c2j7G-PoNtAB3oYYDUhCGw)  ``2019-12-12``
- [Gradle+Groovy提高篇](https://mp.weixin.qq.com/s/yXmYj_1fynLkR0-5FV_Arw)  ``2019-12-13``
- [Maven进行增量构建](https://mp.weixin.qq.com/s/ThQ7j6TS93KJZFqlNx8IQg)  ``2020-04-03``
- [SonarQube8.3中的Maven项目的测试覆盖率报告](https://mp.weixin.qq.com/s/Xhp26jyE1c7Auielz48Llw)  ``2020-10-27``

## plotly可视化

- [MacOS使用pip安装pandas提示Cannot uninstall 'numpy'解决方案](https://mp.weixin.qq.com/s/fIqMAMXRQvf_vBtS5jDsyg)  ``2019-07-28``
- [Python使用plotly生成本地文件教程](https://mp.weixin.qq.com/s/4dJdIP-g3fF40vX7S31jNg)  ``2019-08-21``
- [Python2.7使用plotly绘制本地散点图和折线图实例](https://mp.weixin.qq.com/s/9QWrA0c-STmrmjSkBYWvbQ)  ``2019-08-30``
- [Python可视化工具plotly从数据库读取数据作图示例](https://mp.weixin.qq.com/s/EUtPidiz_r1rpQBH_kudbA)  ``2019-09-16``
- [利用Python+plotly制作接口请求时间的violin图表](https://mp.weixin.qq.com/s/3GdiLaiVRfkxwM3MOG-U8w)  ``2019-09-17``
- [Python+plotly生成本地饼状图实例](https://mp.weixin.qq.com/s/61Qz9Kz-4ruzC0OvIuElpA)  ``2019-09-21``
- [python plotly处理接口性能测试数据方法封装](https://mp.weixin.qq.com/s/NxVdvYlD7PheNCv8AMYqhg)  ``2019-10-04``
- [利用python+plotly 制作接口响应时间Distplot图表](https://mp.weixin.qq.com/s/yrcUW1fFC18newqHcxhVvw)  ``2019-10-11``
- [利用 python+plotly 制作Contour Plots模拟双波源干涉现象](https://mp.weixin.qq.com/s/vNW80BDeHsyjNQrnaBGk3Q)  ``2019-10-15``
- [利用 python+plotly 制作双波源干涉三维图像](https://mp.weixin.qq.com/s/KSeV8VvQXRIg-bnzYoa5qg)  ``2019-10-18``
- [python plotly制作接口响应耗时的时间序列表（Time Series ）](https://mp.weixin.qq.com/s/U8chcVzCjGTdT3T_X5v4kw)  ``2019-10-21``
- [python使用plotly批量生成图表](https://mp.weixin.qq.com/s/l18WfWz-s6qQ1JKKuh_2AQ)  ``2019-10-25``


# 无代码合集


## 理论鸡汤

- [写给所有人的编程思维](https://mp.weixin.qq.com/s/Oj33UCnYfbUgzsBzEm2GPQ)  ``2019-08-05``
- [成为杰出Java开发人员的10个步骤](https://mp.weixin.qq.com/s/UCNOTSzzvTXwiUX6xpVlyA)  ``2019-08-27``
- [测试之《代码不朽》脑图](https://mp.weixin.qq.com/s/2aGLK3knUiiSoex-kmi0GA)  ``2019-09-09``
- [为什么选择软件测试作为职业道路？](https://mp.weixin.qq.com/s/o83wYvFUvy17kBPLDO609A)  ``2019-09-04``
- [自动化测试的障碍](https://mp.weixin.qq.com/s/ZIV7uJp7DzVoKhWOh6lvRg)  ``2019-09-05``
- [自动化测试的问题所在](https://mp.weixin.qq.com/s/BhvD7BnkBU8hDBsGUWok6g)  ``2019-09-09``
- [成为优秀自动化测试工程师的7个步骤](https://mp.weixin.qq.com/s/wdw1l4AZnPpdPBZZueCcnw)  ``2019-09-10``
- [优秀软件开发人员的态度](https://mp.weixin.qq.com/s/0uEEeFaR27aTlyp-sm61bA)  ``2019-09-12``
- [如何正确执行功能API测试](https://mp.weixin.qq.com/s/aeGx5O_jK_iTD9KUtylWmA)  ``2019-09-14``
- [未来10年软件测试的新趋势-上](https://mp.weixin.qq.com/s/9XgpIfXQRuKg1Pap-tfqYQ)  ``2019-09-18``
- [未来10年软件测试的新趋势-下](https://mp.weixin.qq.com/s/k2rZaeHoq4AX19CUzjGRVQ)  ``2019-09-19``
- [自动化测试解决了什么问题](https://mp.weixin.qq.com/s/96k2I_OBHayliYGs2xo6OA)  ``2019-09-20``
- [17种软件测试人员常用的高效技能-上](https://mp.weixin.qq.com/s/vrM_LxQMgTSdJxaPnD_CqQ)  ``2019-09-24``
- [17种软件测试人员常用的高效技能-下](https://mp.weixin.qq.com/s/uyWdVm74TYKb62eIRKL7nQ)  ``2019-09-25``
- [手动测试存在的重要原因](https://mp.weixin.qq.com/s/mW5vryoJIkeskZLkBPFe0Q)  ``2019-10-07``
- [编写测试用例的技巧](https://mp.weixin.qq.com/s/zZAh_XXXGOyhlm6ebzs06Q)  ``2019-11-24``
- [成为自动化测试的7种技能](https://mp.weixin.qq.com/s/e-HAGMO0JLR7VBBWLvk0dQ)  ``2019-10-20``
- [功能测试与非功能测试](https://mp.weixin.qq.com/s/oJ6PJs1zO0LOQSTRF6M6WA)  ``2019-10-23``
- [自动化和手动测试，保持平衡！](https://mp.weixin.qq.com/s/mMr_4C98W_FOkks2i2TiCg)  ``2019-10-28``
- [43种常见软件测试分类](https://mp.weixin.qq.com/s/GTMkcEm-xPtVF7_HxXGKDg)  ``2019-11-01``
- [自动化测试生命周期](https://mp.weixin.qq.com/s/SH-vb2RagYQ3sfCY8QM5ew)  ``2019-11-02``
- [代码审查如何保证软件质量](https://mp.weixin.qq.com/s/osRnG09KDqEojiV3kp2nrw)  ``2019-11-03``
- [TDD测试驱动开发的基础](https://mp.weixin.qq.com/s/diW_2HSbWMEsn8G6uQriOg)  ``2019-11-04``
- [如何在DevOps引入自动化测试](https://mp.weixin.qq.com/s/MclK3VvMN1dsiXXJO8g7ig)  ``2019-11-05``
- [自动化的好处](https://mp.weixin.qq.com/s/7MpWQhtozaTrlUMo1oRSBg)  ``2019-11-07``
- [Web端自动化测试失败原因汇总](https://mp.weixin.qq.com/s/qzFth-Q9e8MTms1M8L5TyA)  ``2019-11-06``
- [测试人员如何成为变革的推动者](https://mp.weixin.qq.com/s/0nTZHBOuKG0rewKAeyIqwA)  ``2019-11-09``
- [探索性测试为何如此重要？](https://mp.weixin.qq.com/s/nebHPfKbCO0f-G24qCh9wA)  ``2019-11-19``
- [5种促进业务增长的软件测试策略](https://mp.weixin.qq.com/s/3mB_DQVD2AZLPs84SmsmuA)  ``2019-11-21``
- [如何选择正确的自动化测试工具](https://mp.weixin.qq.com/s/_Ee78UW9CxRpV5MoTrfgCQ)  ``2019-11-26``
- [如何从测试自动化中实现价值](https://mp.weixin.qq.com/s/dj-sJvGjvFMYANfhIVo8jw)  ``2019-11-28``
- [您如何使用Selenium来计算自动化测试的投资回报率？](https://mp.weixin.qq.com/s/DVSEm0DhoAvYfTWIniabJg)  ``2019-12-03``
- [如何在DevOps中实施连续测试](https://mp.weixin.qq.com/s/snPXkH6WEZ2kteYP_-c5_g)  ``2019-12-10``
- [自动化如何选择用例](https://mp.weixin.qq.com/s/1hH5YIle4YQimJr4iGSWlA)  ``2019-12-17``
- [成功实施自动化测试的优点](https://mp.weixin.qq.com/s/UENdSU-NPa5AOVC9ciiy0Q)  ``2019-12-18``
- [测试人员常用借口](https://mp.weixin.qq.com/s/0k_Ciud2sOpRb5PPiVzECw)  ``2019-12-20``
- [测试自动化的边缘DevTestOps](https://mp.weixin.qq.com/s/kCySRYdCS11CA-lF30AtQA)  ``2019-12-25``
- [筛选自动化测试用例的技巧](https://mp.weixin.qq.com/s/SWNopZLwgpj9yYsVEHEspw)  ``2019-12-27``
- [什么阻碍手动测试发挥价值](https://mp.weixin.qq.com/s/t0VAVyA3ywQsHzaqzSILOw)  ``2019-12-30``
- [未来的QA测试工程师](https://mp.weixin.qq.com/s/ngL4sbEjZm7OFAyyWyQ3nQ)  ``2020-01-03``
- [Web安全检查](https://mp.weixin.qq.com/s/SewUV3GMaNKD2P7g64ctYQ)  ``2020-01-09``
- [关于可用性测试](https://mp.weixin.qq.com/s/aUIg40scOWzbRR89ojJWLg)  ``2020-01-17``
- [如何实施DevOps](https://mp.weixin.qq.com/s/UPIL942eOKR1bY0mbC-42w)  ``2020-01-19``
- [黑盒测试和白盒测试](https://mp.weixin.qq.com/s/5kvrYMWG0vFR3vj-aNY49g)  ``2020-01-20``
- [测试用例中的细节](https://mp.weixin.qq.com/s/wvScTliPwuvH9ReIoDQGNQ)  ``2020-01-23``
- [集成测试、单元测试、系统测试](https://mp.weixin.qq.com/s/LRkxMasRvmDYRVb0_aybtA)  ``2020-01-27``
- [集成测试类型和最佳实践](https://mp.weixin.qq.com/s/sSubzrs3cikLV7rmRQaWEA)  ``2020-01-28``
- [软件测试中质量优于数量](https://mp.weixin.qq.com/s/4FxtVFqialRz6R4680rPAw)  ``2020-01-29``
- [DevOps工具](https://mp.weixin.qq.com/s/4r8FoxQyYZ5naowML5Cw-Q)  ``2020-02-01``
- [2020年Tester自我提升](https://mp.weixin.qq.com/s/vuhUp85_6Sbg6ReAN3TTSQ)  ``2020-02-02``
- [DevOps中的测试工程师](https://mp.weixin.qq.com/s/42Ile_T1BAIp7QHleI-c7w)  ``2020-02-05``
- [敏捷团队的回归测试策略](https://mp.weixin.qq.com/s/Z7dzDdfp5_kxvzBVQ3rEDg)  ``2020-02-09``
- [测试自动化与自动化测试：差异很重要](https://mp.weixin.qq.com/s/6HC1bKesOs4mZYb9nOCHjw)  ``2019-11-10``
- [自动化新手要避免的坑（上）](https://mp.weixin.qq.com/s/MjcX40heTRhEgCFhInoqYQ)  ``2020-02-22``
- [自动化新手要避免的坑（下）](https://mp.weixin.qq.com/s/azDUo1IO5JgkJIS9n1CMRg)  ``2020-02-23``
- [如何成为全栈自动化工程师](https://mp.weixin.qq.com/s/j2rQ3COFhg939KLrgKr_bg)  ``2020-02-25``
- [左移测试](https://mp.weixin.qq.com/s/8zXkWV4ils17hUqlXIpXSw)  ``2020-02-29``
- [选择手动测试还是自动化测试？](https://mp.weixin.qq.com/s/4haRrfSIp5Plgm_GN98lRA)  ``2020-03-02``
- [从单元测试标准中学习](https://mp.weixin.qq.com/s/x0TyMAdPBWYL7JSPAmoQsw)  ``2020-03-01``
- [负载测试很重要](https://mp.weixin.qq.com/s/2q7kNQVJuNwB948ks463CA)  ``2020-03-14``
- [白盒测试扫盲](https://mp.weixin.qq.com/s/s_FvGZTC42GEjaWzroz1eA)  ``2020-03-15``
- [自动化测试项目为何失败](https://mp.weixin.qq.com/s/KFJXuLjjs1hii47C1BH8PA)  ``2020-03-16``
- [简化测试用例](https://mp.weixin.qq.com/s/BhwfDqhN9yoa3Iul_Eu5TA)  ``2020-03-26``
- [敏捷测试二三事](https://mp.weixin.qq.com/s/bKkGWJA3JhvdCjgg6-AVEQ)  ``2020-03-29``
- [软件测试中的虚拟化](https://mp.weixin.qq.com/s/zHyJiNFgHIo2ZaPFXsxQMg)  ``2020-03-30``
- [新词：QA-Ops](https://mp.weixin.qq.com/s/detcY6OVYmzOTUxfwN6CFQ)  ``2020-04-04``
- [生产环境中进行自动化测试](https://mp.weixin.qq.com/s/JKEGRLOlgpINUxs-6mohzA)  ``2020-04-06``
- [所谓UI测试](https://mp.weixin.qq.com/s/wDvUy_BhQZCSCqrlC2j1qA)  ``2020-04-08``
- [预上线环境失败的原因](https://mp.weixin.qq.com/s/jva0Jb2OMarERmTn7Kh2Ng)  ``2020-04-10``
- [自动化策略六步走](https://mp.weixin.qq.com/s/He69k8iCKhTKD1j-yV6M5g)  ``2020-04-16``
- [合格的测试经理必备技能](https://mp.weixin.qq.com/s/gFIYksHMn_bHEwAhmgVzjg)  ``2020-04-17``
- [质量保障的拓展实践](https://mp.weixin.qq.com/s/a3sd0dQnjk3TerOhfo-1ng)  ``2020-05-01``
- [敏捷领导者常见误区](https://mp.weixin.qq.com/s/xdq3CZflRjvDBGDLK4tNFQ)  ``2020-05-04``
- [功能自动化测试策略](https://mp.weixin.qq.com/s/qHmcblN4cD4JK6jT7oU4fQ)  ``2020-05-07``
- [性能测试、压力测试和负载测试](https://mp.weixin.qq.com/s/g26lpd7d7EtpN7pkiqkkjg)  ``2020-05-23``
- [如何维护自动化测试](https://mp.weixin.qq.com/s/4eh4AN_MiatMSkoCMtY3UA)  ``2020-05-24``
- [负载测试最佳实践](https://mp.weixin.qq.com/s/hNj7UsCCvv9TdexAcNFUvg)  ``2020-05-26``
- [有关UI测试计划](https://mp.weixin.qq.com/s/D0fMXwJF754a7Mr5ARY5tQ)  ``2020-05-28``
- [软件测试外包](https://mp.weixin.qq.com/s/sYQfb2PiQptcT0o_lLpBqQ)  ``2020-05-31``
- [避免PPT自动化的最佳实践](https://mp.weixin.qq.com/s/5YgYK4_YLZ1wDDhbwMTGlw)  ``2020-06-02``
- [如何优化软件测试成本](https://mp.weixin.qq.com/s/_eXrzDyNDA6yCRR8nPmzGA)  ``2020-06-03``
- [如何从手动测试转到自动化测试](https://mp.weixin.qq.com/s/EBDTX4AMnn2KTEjL88bOhQ)  ``2020-06-13``
- [Selenium自动化测试技巧](https://mp.weixin.qq.com/s/EzrpFaBSVITO2Y2UvYvw0w)  ``2020-06-22``
- [测试为何会错过Bug](https://mp.weixin.qq.com/s/UFHy8OwZjnMkB70roIS-zQ)  ``2020-06-24``
- [测试用例设计——一切测试的基础](https://mp.weixin.qq.com/s/0_ubnlhp2jk-jxHxJ95E9g)  ``2020-06-29``
- [移动应用测试：挑战，类型和最佳实践](https://mp.weixin.qq.com/s/kYxh6xki69evVDsXDxrYKQ)  ``2020-07-02``
- [敏捷测试中面临的挑战](https://mp.weixin.qq.com/s/vmsW56r1J7jWXHSZdcwbPg)  ``2020-07-04``
- [AI如何影响测试行业](https://mp.weixin.qq.com/s/d6c7u1-lAmsiIQz3UvcGKg)  ``2020-07-07``
- [自动测试失败的5个原因](https://mp.weixin.qq.com/s/bTakAHIcx_WyJIo-tsbvvg)  ``2020-07-18``
- [大促前必做的质量检查](https://mp.weixin.qq.com/s/iOku2wKnlr8pSZO0l9Q3Bw)  ``2020-07-23``
- [测试开发工程师工作技巧](https://mp.weixin.qq.com/s/TvrUCisja5Zbq-NIwy_2fQ)  ``2020-07-24``
- [敏捷回归测试](https://mp.weixin.qq.com/s/_bBQFggkZTTEqcb9R_68OA)  ``2020-08-01``
- [制定质量管理计划指南](https://mp.weixin.qq.com/s/ztXYE8EtwlkUdxnk1cjKVg)  ``2020-08-15``
- [质量管理计划的基本要素](https://mp.weixin.qq.com/s/v8lOioYn01S1F0ex4mmljA)  ``2020-08-17``
- [质量保障的方法和实践](https://mp.weixin.qq.com/s/hU_YCaZB-0a09dOCAVgcpw)  ``2020-08-29``
- [为什么测试覆盖率如此重要](https://mp.weixin.qq.com/s/0evyuiU2kdXDgMDnDKjORg)  ``2020-09-11``
- [自动化测试框架](https://mp.weixin.qq.com/s/vu6p_rQd3vFKDYu8JDJ0Rg)  ``2020-09-21``
- [敏捷中的端到端测试](https://mp.weixin.qq.com/s/cdi4xnEzDLpl9ncQguLuAQ)  ``2020-09-26``
- [自动化测试灵魂三问：是什么、为什么和做什么](https://mp.weixin.qq.com/s/geOejJx79-jTwafG9aXwqA)  ``2020-09-27``
- [基于代码的自动化和无代码自动化](https://mp.weixin.qq.com/s/8Dopihqs4XzpU-sN-I94kw)  ``2020-09-28``
- [物联网测试](https://mp.weixin.qq.com/s/B_JI4DANxoOq4HurxZC65Q)  ``2020-10-14``
- [功能测试知多少](https://mp.weixin.qq.com/s/vTxZLwlvlfIBv892Ji-oLQ)  ``2020-10-14``
- [如何选择自动化测试工具](https://mp.weixin.qq.com/s/yJo-d9bAZDs1Lcp8j7ISRg)  ``2020-10-17``
- [连续测试策略](https://mp.weixin.qq.com/s/0aD_0cUW83oPu3sl7sHNnQ)  ``2020-10-26``
- [如何设置质量检查流程](https://mp.weixin.qq.com/s/PQeXxMZzzU15xSfY5wkVgA)  ``2020-10-28``
- [编写干净的代码之变量篇](https://mp.weixin.qq.com/s/J9rGIe8a2xaLlNJq2nVmzw)  ``2020-10-29``
- [高效Mobile DevOps步骤](https://mp.weixin.qq.com/s/-qc-d_zJ1C9H_Uvd8gJiBw)  ``2020-10-30``
- [回归BUG](https://mp.weixin.qq.com/s/00j-acjPeKQ7uap62WpY3w)  ``2020-11-04``
- [处理回归BUG最佳实践](https://mp.weixin.qq.com/s/R3O2NruPAA2gQf4-3R6aAQ)  ``2020-11-06``
- [自动化测试实践清单](https://mp.weixin.qq.com/s/972WruGsYmkRroquBFoqMg)  ``2020-11-19``
- [自动化测试类型](https://mp.weixin.qq.com/s/GRkN8ozZiWNu21Y3KbVOBA)  ``2020-12-15``
- [无脚本测试](https://mp.weixin.qq.com/s/PVBxk4KEwCmWkB6mOXJFlw)  ``2020-12-23``
- [自动化测试转型挑战及其解决方案](https://mp.weixin.qq.com/s/BixS6jRdF5N_nvmW3_OthQ)  ``2020-12-24``
- [无数据驱动自动化测试](https://mp.weixin.qq.com/s/aCYRGxkzMogLbmACYo6ssw)  ``2021-01-03``
- [为什么自动化测试在敏捷开发中很重要](https://mp.weixin.qq.com/s/AP0wUQZ09NvSqme8e09igQ)  ``2021-01-05``
- [测试模型中理解压力测试和负载测试](https://mp.weixin.qq.com/s/smNLx3malzM3avkrn3EJiA)  ``2021-01-18``
- [移动测试工程师职业](https://mp.weixin.qq.com/s/dhtR4TbQNu5fWpmJkXGivw)  ``2021-01-19``
- [远程测试工作挑战](https://mp.weixin.qq.com/s/LK-GEN4OtuWVGDuG8psmOQ)  ``2021-01-25``
- [自动化测试用例的原子性](https://mp.weixin.qq.com/s/jA5WMHwJcu88nHXWoMBAdQ)  ``2021-02-03``
- [可测性经验分享](https://mp.weixin.qq.com/s/iRtUjESYS3sh3YTD-BWjdA)  ``2021-02-23``
- [敏捷中的回归测试的优化【译】](https://mp.weixin.qq.com/s/nDiZZgA1PIiAUCG_xwA2rA)  ``2021-03-01``
- [敏捷的主要优势【译】](https://mp.weixin.qq.com/s/zkI85TLI37XrPFaQ-pZYMA)  ``2021-03-12``
- [2021年自动化测试流行趋势【译】](https://mp.weixin.qq.com/s/dIZxkNT6mjgRukLBy0AJ6Q)  ``2021-03-15``
- [敏捷团队的自动化测试【译】](https://mp.weixin.qq.com/s/5BvzQvdssTyp8voC9J9www)  ``2021-03-24``
- [自动化测试框架的完整指南【译】](https://mp.weixin.qq.com/s/-EofRDhKLHlTHyh4vvppwg)  ``2021-05-19``
- [测试自动化最佳实践【译】](https://mp.weixin.qq.com/s/Q7LIgKphZytdPcNCaMDXJw)  ``2021-05-26``
- [小白自动化测试指南](https://mp.weixin.qq.com/s/XrSQFi4mxRm-B1u5V3Tn1w)  ``2021-06-02``
- [移动端测试策略【译】](https://mp.weixin.qq.com/s/jCOxGO3Br9Je7F8IWaQ2dg)  ``2021-06-09``
- [如何应对自动化测试挑战【译】](https://mp.weixin.qq.com/s/OP65x3MtzlTKKODLumpxwQ)  ``2021-06-22``
- [2021年移动APP四大测试趋势【译】](https://mp.weixin.qq.com/s/Zs6mknPG4_lJbcZSDV5Wpw)  ``2021-07-09``
- [LT浏览器——响应式网站测试利器](https://mp.weixin.qq.com/s/9V8QdTvRstOcrOIk4V6qNg)  ``2021-08-18``
- [自动化测试框架指南](https://mp.weixin.qq.com/s/hMC28BJxmDkvYPPdDvrXAA)  ``2021-09-01``
- [建立测试自动化策略【译】](https://mp.weixin.qq.com/s/exvq_wgoXedjpNv1t24vhw)  ``2021-09-17``
- [浏览器测试的三大挑战及解决方案【译】](https://mp.weixin.qq.com/s/jeEQ2FoLmN1zHXGwkKP4mg)  ``2021-09-26``
- [持续测试、持续集成、持续交付、持续部署和DevOps](https://mp.weixin.qq.com/s/M-Dc4dSJ4veZwgiIaMDybA)  ``2021-10-21``
- [实现连续测试，要做的事情【译】](https://mp.weixin.qq.com/s/AFw-YYb3RJ7H7EZxv7cVjg)  ``2021-11-02``
- [测试编排必要性](https://mp.weixin.qq.com/s/geJEGWR6ZPn2f6WGwHt8iw)  ``2021-11-09``
- [代码覆盖率VS测试覆盖率](https://mp.weixin.qq.com/s/mrm7oVPu8wYRH4Z3iVEpkA)  ``2021-11-12``
- [单元测试再出发](https://mp.weixin.qq.com/s/98IiKbxy-MfJ1T5kN9bwoQ)  ``2021-11-26``
- [如何开始移动网站测试](https://mp.weixin.qq.com/s/KQLIr5B8OrBhb6YeK-4g1w)  ``2021-12-21``
- [自动化测试指南](https://mp.weixin.qq.com/s/Uwel0C2NxRZlmpQSFtWesA)  ``2022-03-04``
- [所谓测试报告](https://mp.weixin.qq.com/s/LrXgs1mNUpgsru_hPQ2sNg)  ``2022-05-20``
- [国际化和本地化测试](https://mp.weixin.qq.com/s/BSAuHr6JGZk4lDulFnU9hw)  ``2022-05-31``

# UI自动化


## UI自动化

- [自动化测试中java多线程的使用实例](https://mp.weixin.qq.com/s/BNSLaIdcTPTNj1tKpGf6fw)  ``2019-08-03``
- [自动化测试中递归函数的应用](https://mp.weixin.qq.com/s/86602zV9zYblhCRMiUlwdA)  ``2019-08-03``
- [Appium 2.0速览](https://mp.weixin.qq.com/s/mHHSZKYZXQby8YiQBP57hA)  ``2021-03-14``

## UiAutomator

- [android uiautomator一个画心形图案的方法--代码的浪漫](https://mp.weixin.qq.com/s/byfAKHxD2i83VHnuaNgIZA)  ``2019-08-17``
- [android UiAutomator了解源码解决控件bonds\[0,0\]无法点击](https://mp.weixin.qq.com/s/nu2ftXNUSG2_kmZjyhEcVA)  ``2019-08-18``
- [android UiAutomator在清除文本时遇到中文的解决办法](https://mp.weixin.qq.com/s/cNGNCoXsYBSk-MWTWLxF4g)  ``2019-08-18``
- [android UiAutomator获取当前页面某类控件个数的方法](https://mp.weixin.qq.com/s/njb19Sq_Kg4SusAS_eEuug)  ``2019-08-19``
- [android uiautomator自定义监听示例--一个弹出权限设置的监听](https://mp.weixin.qq.com/s/OKKZOf51yq6qY5D6PvN-gg)  ``2019-08-15``
- [如何在Mac OS上使用UiAutomator快速调试类](https://mp.weixin.qq.com/s/jm9d_42jp_BSlv-IW0BpzQ)  ``2019-08-23``
- [UiAutomator测试中如何恢复手机输入法](https://mp.weixin.qq.com/s/o4-zCgbdq6OsHRK9XT14QA)  ``2019-08-25``
- [android UiAutomator基本api的二次封装](https://mp.weixin.qq.com/s/_3jGg3ZYoeyAkjZpy8gWfQ)  ``2019-08-26``
- [android UiAutomator让运行失败的用例重新运行](https://mp.weixin.qq.com/s/tMOPbt1w9tRaKEuIZYKCyg)  ``2019-08-31``
- [利用UiAutomator写一个首页刷新的稳定性测试脚本](https://mp.weixin.qq.com/s/au9hAScsqUdcrh_usu8Pfg)  ``2019-09-01``
- [android UiAutomator长按实现控制按住控件时间的方法](https://mp.weixin.qq.com/s/lOvxAOMh6mmIh3CEV6Fduw)  ``2019-09-02``
- [android UiAutomator自定义快速调试类](https://mp.weixin.qq.com/s/iP2dTOeVkFMzU3dQ06R9GA)  ``2019-09-04``
- [利用UiAutomator写一个自动遍历渠道包关键功能的脚本](https://mp.weixin.qq.com/s/0vg2OlfTy0y4T6sWUG-olA)  ``2019-09-11``
- [android UiAutomator如何根据颜色判断控件的状态](https://mp.weixin.qq.com/s/kldsD3OZ4mJZ5yYQXfXxLw)  ``2019-09-12``
- [android UiAutomator控制多台手机同时运行用例的方法](https://mp.weixin.qq.com/s/z9vgpOQP0wQffmG4C_oBWg)  ``2019-09-27``
- [android UiAutomator使用递归函数写一个让屏幕一闪一闪提醒的方法](https://mp.weixin.qq.com/s/AzXjePdmsgs6QsICZOdPyw)  ``2019-09-30``
- [android UiAutomator获取视频播放进度的方法](https://mp.weixin.qq.com/s/ho070zX9rrLPmh8bZe_HgQ)  ``2019-10-03``

## Selenium

- [selenium2java截图保存桌面](https://mp.weixin.qq.com/s/OUfwsIo635coGONRNccYlg)  ``2019-08-16``
- [selenium2java调用JavaScript方法封装](https://mp.weixin.qq.com/s/t-Xs2Hr9TM2bjDiOqQX2mA)  ``2019-08-25``
- [selenium2java利用mysq解决向浏览器插入cookies时token过期问题](https://mp.weixin.qq.com/s/oAAkDKUGytQjxJLFkod-AQ)  ``2019-08-28``
- [selenium2java 遇到有三个窗口用例的处理办法](https://mp.weixin.qq.com/s/6AJBanVKYwlsNcvsu_25QQ)  ``2019-08-30``
- [selenium2java通过第三方登录绕过知乎登陆验证码](https://mp.weixin.qq.com/s/A5uTtxlg4l4pru2z7v1cug)  ``2019-08-30``
- [selenium2java使用select处理下拉框示例](https://mp.weixin.qq.com/s/FFor451WzuUzINeclGN-Ng)  ``2019-09-03``
- [selenium2java爬虫示例](https://mp.weixin.qq.com/s/vSZzpzEqsCtASSx6iHqxVA)  ``2019-09-05``
- [selenium2java写一个设置秒杀价的脚本](https://mp.weixin.qq.com/s/1ocIOYt3gdGIJrd9v2shhg)  ``2019-09-06``
- [selenium2java基本方法二次封装](https://mp.weixin.qq.com/s/2GaXigt13wa6JgxJkcef5g)  ``2019-09-07``
- [selenium2java一个弹框上传时间日期大杂烩测试用例](https://mp.weixin.qq.com/s/Z8ZeZ-zFy0q0a-e_epT1Kg)  ``2019-09-10``
- [selenium2java造数据例子](https://mp.weixin.qq.com/s/ACO2O5f7Po4Qn242lopMBg)  ``2019-09-15``
- [selenium2java让浏览器停止加载的方法](https://mp.weixin.qq.com/s/aBQdGYys3Bpyf6yigGOCIA)  ``2019-09-24``
- [selenium2java写一个强制刷新页面的方法](https://mp.weixin.qq.com/s/VWW7cH5WSDmw_eCabUh9LQ)  ``2019-09-29``
- [selenium2java通过接口获取并注入cookies](https://mp.weixin.qq.com/s/luLHWxPWSekuDMbnKsfJvg)  ``2019-10-02``
- [Selenium编写自动化用例的8种技巧](https://mp.weixin.qq.com/s/8wRHc_krXNfWclNeOJDNPg)  ``2019-10-29``
- [JUnit中用于Selenium测试的中实践](https://mp.weixin.qq.com/s/KG4sltQMCfH2MGXkRdtnwA)  ``2019-11-08``
- [您如何使用Selenium来计算自动化测试的投资回报率？](https://mp.weixin.qq.com/s/DVSEm0DhoAvYfTWIniabJg)  ``2019-12-03``
- [Selenium 4 Java的最佳测试框架](https://mp.weixin.qq.com/s/MlNyv-kb03gRTcYllxUreA)  ``2019-12-11``
- [Selenium 4.0 Alpha更新日志](https://mp.weixin.qq.com/s/tU7sm-pcbpRNwDU9D3OVTQ)  ``2019-12-15``
- [Selenium 4.0 Alpha更新实践](https://mp.weixin.qq.com/s/yT9wpO5o5aWBUus494TIHw)  ``2019-12-16``
- [JUnit 5和Selenium基础（一）](https://mp.weixin.qq.com/s/ehBRf7st-OxeuvI_0yW3OQ)  ``2020-01-13``
- [JUnit 5和Selenium基础（二）](https://mp.weixin.qq.com/s/Gt82cPmS2iX-DhKXTXiy8g)  ``2020-01-14``
- [JUnit 5和Selenium基础（三）](https://mp.weixin.qq.com/s/8YkonXTYgAV5-pLs9yEAVw)  ``2020-01-15``
- [如何在跨浏览器测试中提高效率](https://mp.weixin.qq.com/s/MB_Wv7yQ6i9BztAZtL4grA)  ``2020-01-16``
- [Selenium Python使用技巧（一）](https://mp.weixin.qq.com/s/39v8tXG3xig63d-ioEAi8Q)  ``2020-02-06``
- [Selenium Python使用技巧（二）](https://mp.weixin.qq.com/s/uDM3y9zoVjaRmJJJTNs6Vw)  ``2020-02-07``
- [Selenium Python使用技巧（三）](https://mp.weixin.qq.com/s/J7-CO-UDspUGSpB8isjsmQ)  ``2020-02-08``
- [Selenium并行测试基础](https://mp.weixin.qq.com/s/OfXipd7YtqL2AdGAQ5cIMw)  ``2020-05-14``
- [Selenium并行测试最佳实践](https://mp.weixin.qq.com/s/-RsQZaT5pH8DHPvm0L8Hjw)  ``2020-05-16``
- [维护Selenium测试自动化的最佳实践](https://mp.weixin.qq.com/s/EMD1aWuzOSfT7j3KeXhJcA)  ``2020-05-29``
- [Selenium自动化测试技巧](https://mp.weixin.qq.com/s/EzrpFaBSVITO2Y2UvYvw0w)  ``2020-06-22``
- [Selenium自动化：代码测试与无代码测试](https://mp.weixin.qq.com/s/gtmLpQ5FCeuzh1SB5mxuvg)  ``2020-07-16``
- [Selenium处理下拉列表](https://mp.weixin.qq.com/s/E2txSVAmDzYIEZWnyAND4g)  ``2020-07-20``
- [Selenium自动化常见问题](https://mp.weixin.qq.com/s/edoxu-QaD0SOw1VqrhCZWA)  ``2020-08-10``
- [Selenium4 IDE，它终于来了](https://mp.weixin.qq.com/s/XNotlZvFpmBmBQy1pYifOw)  ``2020-08-30``
- [Selenium4 IDE特性：无代码趋势和SIDE Runner](https://mp.weixin.qq.com/s/G0S9K0jHsN0P_jxdMME-cg)  ``2020-09-03``
- [Selenium4 IDE特性：弹性测试、循环和逻辑判断](https://mp.weixin.qq.com/s/o4_jIyi9O7s4S3CbTzl5rQ)  ``2020-09-22``
- [Selenium自动化最佳实践技巧（上）](https://mp.weixin.qq.com/s/lZww1azmncMMMHRY0_yKqA)  ``2020-09-13``
- [Selenium自动化最佳实践技巧（中）](https://mp.weixin.qq.com/s/9D0lUZ-XKHiukNeRqp6zOQ)  ``2020-09-16``
- [Selenium自动化最佳实践技巧（下）](https://mp.weixin.qq.com/s/opVik2ZxmTBurIBoa4yipQ)  ``2020-09-17``
- [Selenium等待：sleep、隐式、显式和Fluent](https://mp.weixin.qq.com/s/73BobMq9M12rYMvzxNhRtA)  ``2020-09-25``
- [Selenium自动化的JUnit参数化实践](https://mp.weixin.qq.com/s/WFu5rJaowxhAIcbEoEatkw)  ``2020-10-15``
- [Selenium异常集锦](https://mp.weixin.qq.com/s/DDkaliSVthX-c_KKG-WwNA)  ``2020-10-20``
- [Selenium自动化测试之前](https://mp.weixin.qq.com/s/DKjSnS9sP0SoHUw4OhOikw)  ``2020-12-18``
- [Selenium4前线快报](https://mp.weixin.qq.com/s/pjfD7c0k0U7hM0a2DbzOow)  ``2021-07-14``
- [Selenium4 Alpha-7升级体验](https://mp.weixin.qq.com/s/XC0EwYhuN1zwHCuQS8oEGg)  ``2021-07-16``
- [Selenium 4以后，再不相见的API](https://mp.weixin.qq.com/s/vJcGC3vdpoVCTj5uloMIIw)  ``2021-08-04``
- [Selenium修改HTTP请求头三种方式](https://mp.weixin.qq.com/s/zce9EiJijOp219V5QzCjZA)  ``2021-11-15``
- [Selenium自动化应该避免的测试场景](https://mp.weixin.qq.com/s/jOyKjJG4onmeIaRImlSBjw)  ``2022-04-28``

## APP性能

- [使用monkey测试时，一个控制WiFi状态的多线程类](https://mp.weixin.qq.com/s/P8HVtzHBlj_FcDAAHFKBDg)  ``2019-08-09``
- [java执行和停止Logcat命令及多线程实现](https://mp.weixin.qq.com/s/sUYibRc-muxQoxi48QiaRg)  ``2019-08-10``
- [APP性能测试中获取CPU和PSS数据多线程实现](https://mp.weixin.qq.com/s/NiJSZ8VxpdnarbDJjcJziA)  ``2019-08-10``
- [统计APP启动时间和进入首页时间的多线程类](https://mp.weixin.qq.com/s/IMs6vd3H-HF65Vb-zPwDhw)  ``2019-08-10``
- [如何获取手机性能测试数据FPS](https://mp.weixin.qq.com/s/qZy5AQkNpUXRJk46BHVzaQ)  ``2019-08-23``
- [一个循环启动APP并保持WiFi常开的多线程类](https://mp.weixin.qq.com/s/OgdT4IffDyAdkKmO2SS9iQ)  ``2019-09-07``

## 社群风采

- [如何优雅地屏蔽掉Google搜索结果中视频、新闻、图片等结果](https://mp.weixin.qq.com/s/Iu7pt4Qk3w9sJp3n_UVAeQ)  ``2019-08-02``
- [测试玩梗--欢迎补充](https://mp.weixin.qq.com/s/y_QHbsjFCQVSCfj-A4Usmg)  ``2019-11-30``
- [图解HTTP脑图](https://mp.weixin.qq.com/s/100Vm8FVEuXs0x6rDGTipw)  ``2019-07-26``
- [测试之JVM命令脑图](https://mp.weixin.qq.com/s/qprqyv0j3SCvGw1HMjbaMQ)  ``2019-08-01``
- [好书推荐《Java性能权威指南》](https://mp.weixin.qq.com/s/YWd5Yx6n7887g1lMLTcsWQ)  ``2020-02-28``
- [2019年浏览器市场份额排行榜](https://mp.weixin.qq.com/s/4NmJ_ZCPD5UwaRCtaCfjEg)  ``2019-12-24``
- [JSON基础](https://mp.weixin.qq.com/s/tnQmAFfFbRloYp8J9TYurw)  ``2020-01-30``
- [JMeter吞吐量误差分析](https://mp.weixin.qq.com/s/jHKmFNrLmjpihnoigNNCSg)  ``2020-03-12``
- [JMeter如何模拟不同的网络速度](https://mp.weixin.qq.com/s/1FCwNN2htfTGF6ItdkcCzw)  ``2020-04-07``
- [疫情期间，如何提高远程办公效率](https://mp.weixin.qq.com/s/k_XrdqjGKMshK2Ea-VCNLw)  ``2020-04-22``
- [接口测试视频专题](https://mp.weixin.qq.com/s/4mKpW3QiVRee3kcVOSraog)  ``2020-04-23``
- [Groovy在JMeter中应用专题](https://mp.weixin.qq.com/s/KcxPUDWl7MLQemFRoIV92A)  ``2020-03-27``
- [Java多线程编程在JMeter中应用](https://mp.weixin.qq.com/s/xCnFx5TvIF1SAVNm-aZnxQ)  ``2020-07-19``
- [未来的神器fiddler Everywhere](https://mp.weixin.qq.com/s/-BSuHR6RPkdv8R-iy47MLQ)  ``2020-08-07``
- [Charles报错Failed to install helper解决方案](https://mp.weixin.qq.com/s/LHhMTBhlDM0DrPCvWeU0zA)  ``2020-08-12``
- [测试仓库推介（上）](https://mp.weixin.qq.com/s/zgy6UgNMFcbISD1NhxSAWg)  ``2020-08-25``
- [测试仓库推介（下）](https://mp.weixin.qq.com/s/njnpmRGoEgdxjqkR7c3a6A)  ``2020-08-26``
- [Fiddler Everywhere工具答疑](https://mp.weixin.qq.com/s/2peWMJ-rgDlVjs3STNeS1Q)  ``2020-08-27``
- [Mac上测试Internet Explorer的N种方法](https://mp.weixin.qq.com/s/HeLBPTp2dfs5IlyLMCi90Q)  ``2020-09-01``
- [IntelliJ中基于文本的HTTP客户端](https://mp.weixin.qq.com/s/-9qi_lLVVfxQKEFmcRYFtA)  ``2020-09-23``
- [开源礼节](https://mp.weixin.qq.com/s/EyNules2f9NYdnYAX_NQSw)  ``2020-09-24``
- [弱网测试：最低流畅网速是多少？](https://mp.weixin.qq.com/s/rCji6fZs9yYyk7GyIWvSiA)  ``2020-10-12``
- [接口测试直播回顾](https://mp.weixin.qq.com/s/B8ih9sswaE-OWuVib6C16g)  ``2020-10-20``
- [SpotBugs报错no Groovy library is defined解决办法](https://mp.weixin.qq.com/s/XxvuVS2TmlqT5-b22vObYQ)  ``2020-12-08``
- [推荐好书：不要总是谦卑地弯着腰](https://mp.weixin.qq.com/s/mYNN9jSaikOF5aJEkb-Bug)  ``2020-12-10``
- [假期思考题](https://mp.weixin.qq.com/s/3DOnkmYDlwk-XKg4ge3ZUw)  ``2021-02-09``
- [甩锅技能+1](https://mp.weixin.qq.com/s/nMwlfXZoDcRRPHcTKpvfNg)  ``2021-02-27``
- [不要浪费自己的求知欲](https://mp.weixin.qq.com/s/WO0aQqmhU_xGUpWvYwOqUA)  ``2021-03-24``
- [Intellij运行Java程序启动等待BUG分享](https://mp.weixin.qq.com/s/Y6-CapeSV9UofwggHu_XbQ)  ``2021-03-31``
- [721法则推介](https://mp.weixin.qq.com/s/kyZB7S83jix9ivePxL3yCA)  ``2021-04-07``
- [高潜力员工自检表](https://mp.weixin.qq.com/s/jvKrWz9sEzNyM1Pi1GbFtg)  ``2021-04-13``
- [学习笔记杂谈](https://mp.weixin.qq.com/s/OCpM0qfxD3wudZ00KQgKvg)  ``2021-04-21``
- [做最积极下班的那个](https://mp.weixin.qq.com/s/KIMyrqkqpryl3YeUSBAzmg)  ``2021-05-09``
- [五一学习心得](https://mp.weixin.qq.com/s/N2w3M1mwMJSMBA-EzccoCg)  ``2021-05-13``
- [想点大事--法律是种思维方式](https://mp.weixin.qq.com/s/b0cCAleJPAQDX7c6jgD87Q)  ``2021-05-27``
- [把工作讲给家人听](https://mp.weixin.qq.com/s/CEH-FPPQU1b_UPz1oU0N3A)  ``2021-09-07``
- [敬畏用户](https://mp.weixin.qq.com/s/3b-K2x4knvzaw6O2y--LmA)  ``2021-10-09``
- [从错误中学习](https://mp.weixin.qq.com/s/FUBpKxrr8RSc31_9gDHrNQ)  ``2021-10-26``
- [JSON必知必会【PDF+视频教程】](https://mp.weixin.qq.com/s/gJPUTHUW0WVUe6FUyZMqjQ)  ``2021-10-29``
- [把选择题变成问答题](https://mp.weixin.qq.com/s/eUybsDHxFkCq7OE0fidb7w)  ``2021-11-03``
- [无代码Web UI自动化工具Automa初体验](https://mp.weixin.qq.com/s/Bo7sPU8_iPqBIuNqVRW2Wg)  ``2021-11-06``
- [莫要寻找可能不存在的答案](https://mp.weixin.qq.com/s/Ky3zettxSCGbQOsfeEGbAg)  ``2021-12-03``
- [自动化测试是好职业么？](https://mp.weixin.qq.com/s/sEYWZIaxzU7GQ0HUubhsJw)  ``2020-05-26``
- [推倒重来的觉悟](https://mp.weixin.qq.com/s/wV6rND9z-CmKHvJMaHtnxg)  ``2021-12-14``
- [卷王本卷](https://mp.weixin.qq.com/s/tsmOEC9VHu6TPYfuyKEGyg)  ``2021-09-28``
- [Jira API的踩坑记](https://mp.weixin.qq.com/s/Mw9AzcK8tlceXEgaKqLVeg)  ``2021-09-10``
- [Http请求基础笔记【before接口测试】](https://mp.weixin.qq.com/s/GUXFZ-sdEYzSpw98k-P3pQ)  ``2021-08-11``
- [超万字回顾FunTester的前世今生](https://mp.weixin.qq.com/s/bEW6DK86qwgAWnjdkUGiZA)  ``2021-08-03``
- [FunTester分享会第二期视频回顾](https://mp.weixin.qq.com/s/sZ7WW1twRrTRH2ym59DnHw)  ``2021-06-24``
- [如何学习Java【FunTester分享会第三期回顾】](https://mp.weixin.qq.com/s/ksXySWR5Lk3SrpiuSn_M1w)  ``2021-07-08``
- [「程序员35岁被淘汰」已经22岁了](https://mp.weixin.qq.com/s/jPLL3yeL-LAPifwmV9_0aw)  ``2022-01-08``
- [如何突破职业瓶颈](https://mp.weixin.qq.com/s/A07I_Kn7FlEPG2wSKk8zOg)  ``2022-01-13``
- [坚持做正确的事情](https://mp.weixin.qq.com/s/FnuseXqOApMPcaL7uzHybA)  ``2022-04-22``
- [红利、辛苦钱、利润和工资【读书笔记】](https://mp.weixin.qq.com/s/6FxetG9OcjL7T8RT55DKWg)  ``2022-06-04``

## 社区风采

- [Linux文件系统和vim命令](https://mp.weixin.qq.com/s/xzModz-upKv5CbAjtWOxpw)  ``2021-04-16``
- [Linux/Mac简单又强大的基础工具（一）](https://mp.weixin.qq.com/s/zk3MCiy01Z_JRyTvU1oDgg)  ``2021-04-21``
- [《漫画算法》读书心得（一）](https://mp.weixin.qq.com/s/eSB6TuDhT_0YO1VWb_wicA)  ``2021-06-11``
- [Jmeter基于webscoket测试后台服务接口实战](https://mp.weixin.qq.com/s/yWwl1R-yaDWRpMQXtx_ebA)  ``2021-06-10``
- [Tcloud 云测平台--集大成者](https://mp.weixin.qq.com/s/29sEO39_NyDiJr-kY5ufdw)  ``2019-09-11``
- [Tester专用名词：测试方案&测试计划&测试报告](https://mp.weixin.qq.com/s/w2IMNIl2qd5adxv4g6p9YA)  ``2021-06-30``
- [初遇Postman，SayHi的三种方式](https://mp.weixin.qq.com/s/-UFIJ0ECPvm8LmiufNBwIg)  ``2021-07-01``
- [Airtest-android快速入门与实战](https://mp.weixin.qq.com/s/tBh8ko8vpfVn8bgDKVUAgg)  ``2021-07-12``
- [安卓APP测试知识大全【面试储备】](https://mp.weixin.qq.com/s/ZTHukqpwD0JYTB1cndAOcA)  ``2021-08-15``
- [颇具年代感的《JMeter中文操作手册》](https://mp.weixin.qq.com/s/5Yx9nUkndsHQ0mdyuqUuHQ)  ``2021-09-06``
- [140道面试题目（UI、Linux、MySQL、API、安全）](https://mp.weixin.qq.com/s/TNI0g_ldTY_t5oWfVgAvXg)  ``2021-09-19``
- [Postman进阶](https://mp.weixin.qq.com/s/97zmzMhmbwanNmUMlFR84g)  ``2021-09-30``
- [Java正则学习笔记](https://mp.weixin.qq.com/s/54OeEbFSgPdGyjx3_gAI8w)  ``2021-10-07``
- [大厂三面都问什么](https://mp.weixin.qq.com/s/qqb6WXgsrkK5x9H7y8TlvQ)  ``2021-10-13``
- [分享一份Fiddler学习包](https://mp.weixin.qq.com/s/3J6hKKqt9GTlo4Z9SiJiLw)  ``2021-10-14``
- [腾讯子公司三轮面试经历](https://mp.weixin.qq.com/s/c_nw30diunkROiub3xpQmg)  ``2021-10-16``
- [性能瓶颈调优【粉丝投稿】](https://mp.weixin.qq.com/s/GS2scJtLCYIKVs8pOBOCNg)  ``2021-10-31``
- [分享11份笔试题](https://mp.weixin.qq.com/s/UhKplAuM1g20sJpfyRAhHg)  ``2021-12-02``
- [接口自动化面试题【思路分享】](https://mp.weixin.qq.com/s/t7oHNLW_tPkStXMHJjYQUA)  ``2021-12-15``
- [2022年度计划手册模板](https://mp.weixin.qq.com/s/9irCVAQ10PhyAY0wPxG2WQ)  ``2022-01-05``
- [基于爬虫的测试自动化经验分享](https://mp.weixin.qq.com/s/0Wxia5svKnDQoalzZvnFVg)  ``2022-03-17``
- [学习编程是最好的复利方式](https://mp.weixin.qq.com/s/GQHlaOT2PDtwu2tns4VaXg)  `` 2022-03-30``
- [逃离过度努力陷阱](https://mp.weixin.qq.com/s/x4mrGilxzzXY-68fF2eqrA)  ``2022-04-11``
- [动态压测模型让工作更轻松](https://mp.weixin.qq.com/s/qYlgZfukyVqoEj8nMVC0gA)  ``2022-04-12``

# 视频专区

## 接口测试视频

- [FunTester测试框架视频讲解（序）](https://mp.weixin.qq.com/s/CJrHAAniDMyr5oDXYHpPcQ)  ``2020-03-17``
- [获取HTTP请求对象--测试框架视频讲解](https://mp.weixin.qq.com/s/hG89sGf96GcPb2hGnludsw)  ``2020-03-18``
- [发送请求和解析响应—测试框架视频解读](https://mp.weixin.qq.com/s/xUQ8o3YuZOChXZ2UGR1Kyw)  ``2020-03-19``
- [JSONObject对象基本操作--视频讲解](https://mp.weixin.qq.com/s/MQtcIGKwWGEMb2XD3zmAIQ)  ``2020-03-20``
- [GET请求实践--测试框架视频讲解](https://mp.weixin.qq.com/s/_ZEDmRPXe4SLjCgdwDtC7A)  ``2020-03-21``
- [POST请求实践--视频演示](https://mp.weixin.qq.com/s/g0mLzMQ4Br2e592m3p68eg)  ``2020-03-23``
- [如何处理header和cookie--视频演示](https://mp.weixin.qq.com/s/MkwzT9VPglSnOxY7geSUiQ)  ``2020-03-25``
- [FunRequest类功能--视频演示](https://mp.weixin.qq.com/s/WGS6ZwAvw7X4MC004Gz4pA)  ``2020-03-26``
- [接口测试业务验证--视频演示](https://mp.weixin.qq.com/s/DH8HDmaritXQnkBIFOadoA)  ``2020-03-28``
- [自动化测试项目基础--视频讲解](https://mp.weixin.qq.com/s/n9zu4OLyj7FbNsV0bYlOYg)  ``2020-03-31``
- [JSONArray基本操作--视频演示](https://mp.weixin.qq.com/s/OosDbRoknMe1riaPc3hhLg)  ``2020-04-02``
- [自动化项目基类实践--视频演示](https://mp.weixin.qq.com/s/IdvSi-GDtE5nqGnR-_4LWA)  ``2020-04-05``
- [模块类和自动化用例实践--视频演示](https://mp.weixin.qq.com/s/Y_A8M7KHmdlJJOD4B4rN4Q)  ``2020-04-09``
- [性能框架多线程基类和执行类--视频讲解](https://mp.weixin.qq.com/s/8Dh-5XfvX8Fm4IqmzbtY6Q)  ``2020-04-11``
- [定时和定量压测模式实现--视频讲解](https://mp.weixin.qq.com/s/l_4wCjVM1fAVRHgEPrcrwg)  ``2020-04-13``
- [基于HTTP请求的多线程实现类--视频讲解](https://mp.weixin.qq.com/s/8SG1xtzq8ArY84Bxm_SNow)  ``2020-04-18``

## Groovy专题

- [Groovy简介](https://mp.weixin.qq.com/s/aQXfRt70eAIJa3-URyfq7w)  ``2022-03-23``
- [Groovy环境及基础语法](https://mp.weixin.qq.com/s/pIqU5nw04kdeUwByhMS8EQ)  ``2022-04-07``
- [Groovy Gstring演示](https://mp.weixin.qq.com/s/H_kAvTo45rhxflgHUf2SgA)  `` 2022-04-15``
- [Groovy as关键字演示](https://mp.weixin.qq.com/s/MsGymlR0EQb-9n7Zy5WjPQ)  `` 2022-04-27``
- [Groovy遍历实践](https://mp.weixin.qq.com/s/1ti37WDNlurou2ZCOHoC6Q)  `` 2022-05-10``
- [Groovy正则表达式](https://mp.weixin.qq.com/s/yR0WOp_uZSjYrJOyNcsVuw)  ``2022-05-16``
- [Groovy闭包实践](https://mp.weixin.qq.com/s/-NsuEkkbEleCbz00v7-sKg)  `` 2022-05-17``

## 其他视频

- [动态压测模型动态增减功能演示](https://mp.weixin.qq.com/s/82QtYBPteNjRykneYsU4GA)  ``2021-11-05``
- [Automa基础功能演示](https://mp.weixin.qq.com/s/NlUuiMzNSMl_saM0KoeNNQ)  ``2021-11-06``
- [DelayQueue基础功能演示](https://mp.weixin.qq.com/s/2nNkVn0unFUrNY5-Jqj4yQ)  ``2021-11-09``
- [ThreadLocal类基础功能演示](https://mp.weixin.qq.com/s/0ZGoiwZAtH2_tar7A3qeow)  ``2021-11-14``
- [Groovy相比Java语法简洁性](https://mp.weixin.qq.com/s/euFggxcZweOJW3mYsu_5CQ)  ``2021-11-21``
- [如何在JMeter中操作Redis](https://mp.weixin.qq.com/s/2N6CffZ_UyhFIX7ooe_NEg)  ``2021-11-24``
- [使用Groovy metaclass进行Java热更新演示](https://mp.weixin.qq.com/s/25FXzDQ4TGUQ0PzUMRcwAQ)  ``2021-11-25``
- [控制台彩色输出](https://mp.weixin.qq.com/s/OALk-1iuRJDYL8JwlXckcQ)  ``2021-12-04``
- [反射访问和修改private变量](https://mp.weixin.qq.com/s/GXGDbNyWdizG8mF90RPTHQ)  ``2021-12-17``
- [Java线程同步三剑客之CountDownLatch](https://mp.weixin.qq.com/s/Fj08m1oK9L24ohh0906lUQ)  ``2021-12-22``
- [Java线程同步三剑客之CyclicBarrier](https://mp.weixin.qq.com/s/L9kQd4wbWf7qcvo9VM-4fQ)  ``2021-12-24``
- [Java线程同步三剑客之Phaser](https://mp.weixin.qq.com/s/d53XzPVmW3hcu2wzki07Kg)  ``2021-12-25``
- [Disruptor高性能队列常用API演示](https://mp.weixin.qq.com/s/3ztb3_ScXfQQheDZRAwEcQ)  ``2021-12-28``
- [Java并发编程ReentrantLock类常用功能演示](https://mp.weixin.qq.com/s/vfYW00gx0iMJGnBTYqOe1A)  ``2022-01-04``
- [千万级日志回放引擎设计稿视频版](https://mp.weixin.qq.com/s/k5ycJPT4beIamdrhtBsqsw)  ``2022-01-05``
- [Java热更新带参方法和第二种写法演示](https://mp.weixin.qq.com/s/t4PI20kmpRx_QPsT9ezjfQ)  ``2022-01-06``
- [「程序员35岁被淘汰」已经22岁了视频版](https://mp.weixin.qq.com/s/wCO0zAOj4hkg6ET9qI67Zg)  ``2022-01-08``
- [automa实现批量删除后台文章](https://mp.weixin.qq.com/s/qFV9wFLC4DGBRTlxp82j0w)  ``2022-01-19``
- [Java自定义DNS解析器实践](https://mp.weixin.qq.com/s/rQm99V_P5GEHNSBFRta0ZA)  ``2022-02-06``
- [Java自定义DNS解析器负载均衡实践视频版](https://mp.weixin.qq.com/s/JqkzaVZNo3noGdzshMuSzQ)  ``2022-02-11``
- [Go语言HTTP自定义DNS解析与负载均衡实现视频版](https://mp.weixin.qq.com/s/S6Y_xQr5MNttg1BAyOzwBQ)  ``2022-03-01``
- [gRPC测试开发实践【Java视频版】](https://mp.weixin.qq.com/s/pChC_bmm6IWQecWSYFKGjg)  `` 2022-05-06``
- [通用池化框架commons-pool2基础实践](https://mp.weixin.qq.com/s/yOo7AAdSVHN4cxY4jKc5-w)  `` 2022-05-27``

## arthas诊断工具

- [arthas快速入门视频演示](https://mp.weixin.qq.com/s/Wl5QMD52isGTRuAP4Cpo-A)  ``2020-04-20``
- [arthas进阶thread命令视频演示](https://mp.weixin.qq.com/s/XuF7Nr1sGC3diIn50zlDDQ)  ``2020-04-23``
- [arthas命令jvm,sysprop,sysenv,vmoption视频演示](https://mp.weixin.qq.com/s/87BsTYqnTCnVdG3a_kBcng)  ``2020-04-25``
- [arthas命令logger动态修改日志级别--视频演示](https://mp.weixin.qq.com/s/w724P9B12eTC9rMbavwsMA)  ``2020-04-27``
- [arthas命令sc和sm视频演示](https://mp.weixin.qq.com/s/Ga63sjW_bOKQqfnA5LTb9w)  ``2020-04-30``
- [arthas命令ognl视频演示](https://mp.weixin.qq.com/s/cMCaXFwjp6QHFq40TvP4bQ)  ``2020-05-03``
- [arthas命令redefine实现Java热更新](https://mp.weixin.qq.com/s/2HUXfJhoUfg4yMzSoRHK9w)  ``2020-05-05``
- [arthas命令monitor监控方法执行](https://mp.weixin.qq.com/s/7-oe3UoTY8bzpi89tIKvQQ)  ``2020-05-08``
- [arthas命令watch观察方法调用（上）](https://mp.weixin.qq.com/s/6fMKP7H4Q7ll_0v-wyN19g)  ``2020-05-12``
- [arthas命令watch观察方法调用（下）](https://mp.weixin.qq.com/s/-r2kufxdOjRb2TgF2HPskg)  ``2020-05-14``
- [arthas命令trace追踪方法链路](https://mp.weixin.qq.com/s/bzkdKZugkOl8C-_xTw92YA)  ``2020-05-18``
- [arthas命令tt方法时空隧道](https://mp.weixin.qq.com/s/mDczYmVdSmL5ZbK7bb8i0A)  ``2020-05-19``
