# 分支简介

该分支为主分支,其他分支停止更新

**FunTester**测试框架支持功能:

### 功能

支持能力受限于我自己工作经验，仅对以下功能进行过封装和兼容。

* HTTP协议
* MySQL
* Redis
* dubbo
* Socket.IO & WebSocket
* MongoDB

### 性能

* 同上
* 凡事支持Java的，**FunTester**都支持

### 自动化

**事无定形**，需要依旧具体情况自己实现，每个项目的框架都不一样。思路请参考下图框架架构图内容。

## [GitHub地址](https://github.com/JunManYuanLong/FunTester)

联系地址：FunTester@88.com

[FunTester测试框架架构图](http://pic.automancloud.com/structure.html)

[FunTester测试项目架构图](http://pic.automancloud.com/project.html)

![FunTester测试框架架构图](http://pic.automancloud.com/structure.png)

![FunTester测试项目架构图](http://pic.automancloud.com/project.png)